#*=============================================================================
#* Script Name:  CaptureSQLSplunk.ps1
#*=============================================================================
#* Created:     02/14/2020
#* Author:      Rick Sellers
#* Company:     Premise Health'
#
#*=============================================================================
# Description
#*=============================================================================
# Captures SQL Server Audit records and exports them to a file for Splunk agent
#  to pick up and process.
#
#*=============================================================================
# Parameters
#*=============================================================================
#  ExceptionFilePath [string] - Specifies the path and filename of the text file
#   containing a list of accounts whose audit records should be excluded from
#   the export file.
#
#  BookmarkFilePath [string] - Specifies the path and filename for the bookmark file.
#
#  OutputFilePath [string] - Specifies the path to which to write the output file.
#
#  ExcludeLocalTraffic [switch] - Specifies whether to exclude audit records where
#    the SQL Server service account is the server_principal_name.
#
#*=============================================================================
# Example
#*=============================================================================
#  CaptureSQLSplunk.ps1 -ExceptionFilePath 'C:\Splunk\PowerShell\SplunkExceptions.txt' -BookmarkFilePath 'C:\Splunk\PowerShell\BookmarkFile.txt' -OutputFilePath 'C:\Splunk\OutputFiles'  
#
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date:        [DATE_MDY]
#* Time:        [TIME]
#* Issue:
#* Solution:
#*
#*=============================================================================

<#
.SYNOPSIS
    CaptureSQLSplunk.ps1 -ExceptionFilePath 'C:\Splunk\PowerShell\SplunkExceptions.txt' -BookmarkFilePath 'C:\Splunk\PowerShell\BookmarkFile.txt' -OutputFilePath 'C:\Splunk\OutputFiles' 

.DESCRIPTION
    Captures SQL Server Audit records and exports them to a file for Splunk agent to pick up and process. 

.PARAMETER ExceptionFilePath
    Specifies the path and filename of the text file containing a list of accounts whose audit records 
    should be excluded from the export file.

.PARAMETER BookmarkFilePath
    Specifies the path and filename for the bookmark file.

.PARAMETER OutputFilePath
    Specifies the path to which to write the output file.

.PARAMETER ExcludeLocalTraffic
    Specifies whether to exclude audit records where the SQL Server service account is the server_principal_name.

.OUTPUTS
    Messages on host console

.EXAMPLE
    CaptureSQLSplunk.ps1 -ExceptionFilePath 'C:\Splunk\PowerShell\SplunkExceptions.txt' -BookmarkFilePath 'C:\Splunk\PowerShell\BookmarkFile.txt' -OutputFilePath 'C:\Splunk\OutputFiles' 

#>


Param
(
    [Parameter(Mandatory=$false)]
    [string]$ExceptionFilePath = 'C:\Splunk\PowerShell\SplunkExceptions.txt',
    [Parameter(Mandatory=$false)]
    [string]$BookmarkFilePath = 'C:\Splunk\PowerShell\BookmarkFile.txt',
    [Parameter(Mandatory=$false)]
    [string]$OutputFilePath = 'C:\Splunk\OutputFiles',
    [Parameter(Mandatory=$false)]
    [switch]$ExcludeLocalTraffic
)

try
{
    import-module sqlserver

    # verify parameters
    if ($ExceptionFilePath -eq '')
    {
        throw 'No Exceptions File specified.  Terminating.'
        return
    }
    
    if ($BookmarkFilePath -eq '')
    {
        throw 'No Bookmark File specified.  Terminating.'
        return
    }
    
    if ($OutputFilePath -eq '')
    {
        throw 'No Output Path specified.  Terminating.'
        return
    }
    

    # Declare variables
    [string]$SQL = ''
    [string]$AuditFileName = ''
    [System.Data.DataSet]$DataSet = $null
    [System.Data.DataTable]$DataTable = $null
    [System.Data.DataRow]$DataRow = $null
    [string]$InitialFile = $null
    [string]$Offset = $null
    [System.Object[]]$BookmarkData = @(2)
    [bool]$Reset = $false

    # Begin building the exceptions list of accounts to ignore
    [string]$Exceptions = '('

    # =====================================================================================================
    # Process the ExcludeLocalTraffic switch parameter 
    # =====================================================================================================
    # Add the local service account to the exceptions list if the switch parameter is $true
    if ($ExcludeLocalTraffic)
    {
        [string]$LocalServiceAccount = (Invoke-Sqlcmd -Query 'Select TOP (1) service_account FROM sys.dm_server_services' -ErrorAction Stop -ErrorVariable ErrorVar).ItemArray[0]
        $Exceptions += "'$LocalServiceAccount', "
    }

    # =====================================================================================================
    # Process the exceptions file.  
    # =====================================================================================================
    # Get out if it is missing since it should always contain PREMISEHEALTH\SVC-P-SQLSENTRYMON
    if(Test-Path -Path $ExceptionFilePath)
    {
        # Read the exceptions file
        $ExceptionList = Get-Content -LiteralPath $ExceptionFilePath -ErrorAction Stop -ErrorVariable ErrorVar
    }
    else
    {
        throw " Unable to find Exception File at $ExceptionFilePath."
    }

    # build the $exceptions string value for use in the SQL statement
    for([int]$i = 0; $i -lt $ExceptionList.Length; $i++)
    {
        [string]$Exception = $ExceptionList[$i]
        if ($i -eq 0)
        {
            $Exceptions += "'$Exception'"
        }
        else
        {
            $Exceptions += ", '$Exception'"
        }
    }
    $Exceptions += ")"

    # =====================================================================================================
    # Process the AuditBookmark file
    # =====================================================================================================
    if((Test-Path -Path $BookmarkFilePath) -eq $true)
    {
        $BookmarkData = Get-Content -LiteralPath $BookmarkFilePath
        [string] $temp = $BookmarkData[0]
        $InitialFile = "'$temp'"
        $Offset = $BookmarkData[1]
    }
    else
    {
        $InitialFile = 'DEFAULT'
        $Offset = 'DEFAULT'
    }

    # get audit file path and file name
    $SQL = "SELECT TOP(1) audit_file_path FROM sys.dm_server_audit_status WHERE name LIKE 'DBAMaintAudit_%' AND status_desc = 'STARTED' ORDER BY audit_id DESC"

    $DataRow = Invoke-Sqlcmd -ServerInstance . -Database master -Query $SQL -ErrorAction Stop -ErrorVariable ErrorVar
    $AuditFileName = $DataRow.ItemArray[0].ToString() 

    # verify we have a valid audit file, and throw an error if not
    if ($null -eq $AuditFileName)
    {
        throw 'No Server Audit is currently running.  Terminating.'
    }

    # get just the audit path
    [string]$AuditFilePath = $AuditFileName.Substring(0,$AuditFileName.LastIndexOf('\')) 


    # Process the tail of the previous day's log file and reset the process for a new audit file 
    # since the audit file is cycled.  This should be at 2:00 AM each day.  Therefore, if the time 
    # is between 2:01 AM and 2:05 AM, we delete the BookmarkFile.txt to force a reset to the 
    # current day and catch up.
    [datetime]$datetime = Get-Date
    if(($datetime.hour) -eq 2 -and ($datetime.Minute -ge 1 -and $datetime.Minute -le 5))
    {
        [string]$Date = $datetime.AddDays(-1).ToString('yyyyMMdd')
        Remove-Item -Path $BookmarkFilePath -Force
        $Reset = $true
    }
    else
    {
        # We need to get the date and adjust for the 2:00 AM recycle of the audit log.  First,
        # we get the date.  If the hour part of the date is less than 2, we subtract one day 
        # from the current date to get the date in the audit file name.
        if (($datetime.Hour) -lt 2)
        {
            [string]$Date = $datetime.AddDays(-1).ToString('yyyyMMdd')
        }
        else
        {
            [string]$Date = $datetime.ToString('yyyyMMdd')
        }
    }

    # Build SQL text to execute to read the audit file
    $SQL = 
        "SELECT 
            event_time,
            audit_file_offset,
            file_name,
            sequence_number,
            action_id,
            succeeded,
            session_id,
            server_principal_name,
            server_instance_name,
            [database_name],
            [schema_name],
            [object_name],
            CASE
                WHEN action_id IN ('LGIS', 'LGO') THEN ''
                ELSE [statement]
            END AS [statement] 
        FROM fn_get_audit_file('$AuditFilePath\DBAMaintAudit_*_$Date*.sqlaudit', $InitialFile, $Offset)
        WHERE server_principal_name NOT IN $Exceptions"

    # Write the SQL to the output file
    Write-Output $SQL
    $DataRow = $null

    # Execute the SQL to ge the audit records in a DataSet object -OutputAs DataSet 
    $DataSet = Invoke-Sqlcmd -ServerInstance . -Database DBAMaint -Query $SQL -OutputAs DataSet -ErrorAction Stop -ErrorVariable ErrorVar -QueryTimeout 65535

    # Ensure that we have data to process
    if ($null -ne $DataSet)
    {
        # select the first (only) table from the DataSet
        [System.Data.DataTable]$DataTable = $Dataset.Tables[0] 

        # Write the DataTable row count to the output file
        Write-Output $DataTable.Rows.Count

        # write the bookmark.txt file if we are not in the reset run at 2:01 AM
        if ($Reset -eq $false)
        {
            # Set the DataRow object to the last row in the DataTable and get the file name and offset to 
            #  update the bookmark record
            $DataRow = $DataTable.Rows[$DataTable.Rows.Count-1]

            [string]$InitialFileName = $DataRow.Item('file_name')
            [bigint]$Offset = $DataRow.Item('audit_file_offset')

            # Update the bookmark file - on the second value, we append and suppress the new line character
            $InitialFileName | Out-File -LiteralPath $BookmarkFilePath
            $Offset | Out-File -LiteralPath $BookmarkFilePath -Append -NoNewline
        }

        # Build export file name
        [string]$ExportFileName = "$OutputFilePath\SplunkSQLAuditData_" + (Get-Date).ToString('yyyyMMdd_HHmmss') + ".xml"

        # Export the DataTable rows to XML
        $DataTable.Rows | Export-Clixml -Path $ExportFileName -Force
    }
}
Catch
{
    # Dump both the $_ and the $Errorvar values to the output file
    Write-Output $_
    Write-Output $ErrorVar
}


USE [master]
GO

CREATE LOGIN [rick] WITH PASSWORD=N'rick', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO

CREATE USER [rick] FOR LOGIN [rick]
GO

ALTER USER [rick] WITH DEFAULT_SCHEMA=[dbo]
GO

GRANT VIEW SERVER STATE TO rick
GO

GRANT CONTROL SERVER TO rick
GO

GRANT SELECT ON master.sys.fn_get_audit_file TO Rick
GO

GRANT SELECT ON master.sys.dm_server_audit_status TO Rick
GO
/*=========================================================
NAME:			dbo.BuildFailedDatabaseList
DESCRIPTION:	Reads the dbo.DatabaseRestoreHistory table and returns
				a list of Server/Database combinations which failed
				the Database Restore and CHECKDB process since the previous 
				Monday.  The list includes any Server/Database 
				combinations that failed on that Monday as well.
INPUTS:			None
OUTPUTS:		None
RETURN:			Recordset of Server/Database combinations
RESULTS:		None
ERRORS:			No Custom Errors
DEPENDENCIES:	dbo.DatabaseRestoreHistory
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20191115	Initial Author
									
=========================================================
--DEBUG--

EXEC dbo.BuildFailedDatabaseList
    
=========================================================*/
CREATE or ALTER PROCEDURE dbo.BuildFailedDatabaseList
AS
BEGIN
	-- determine date of previous Monday
	SET DATEFIRST 1	--Monday
	DECLARE @DoW INT
	DECLARE @CurrentDate DATE
	DECLARE @PrevMondayDate DATE

	SET @CurrentDate = GETDATE()
	SET @DoW = (SELECT DATEPART(dw, @CurrentDate))
	SET @PrevMondayDate = DATEADD(DAY, -@DoW+1, @CurrentDate)
	PRINT @PrevMondayDate

	-- Declare work table
	DECLARE @Databases TABLE
	(
		SourceServer		VARCHAR(128),
		SourceDomain		VARCHAR(128),
		SourceInstance		VARCHAR(128),
		SourceDatabaseName	VARCHAR(128),
		EndRunDate			DATETIME,
		LastRunResult		VARCHAR(50)
	)

	-- Select all Server/Database combinations since the 
	-- last Monday with their most recent end date
	INSERT INTO @Databases
		SELECT
			SourceServer,
			SourceDomain,
			SourceInstance,
			SourceDatabaseName,
			MAX(EndRunDate),
			NULL
		FROM dbo.Databaserestorehistory
		WHERE EndRunDate >= @PrevMondayDate
		GROUP BY SourceServer, SourceDomain, SourceInstance, SourceDatabaseName

	-- Update the work table with the result of each combination
	-- that matches their latest end date
	UPDATE @Databases SET
		LastRunResult = drh.LastRunResult
	FROM @Databases d
		INNER JOIN dbo.DatabaseRestoreHistory drh ON
			d.SourceServer = drh.SourceServer
			AND
			d.SourceDomain = drh.SourceDomain
			AND
			d.SourceInstance = drh.SourceInstance
			AND
			d.SourceDatabaseName = drh.SourceDatabaseName
			AND
			d.EndRunDate = drh.EndRunDate

	-- Delete and combinations that succeeded the last time they ran
	DELETE FROM @Databases WHERE LastRunResult = 'Success'

	-- Select results for return
	SELECT 
		SourceServer,
		Sourcedomain,
		Sourceinstance,
		SourceDatabaseName
	FROM @Databases

END
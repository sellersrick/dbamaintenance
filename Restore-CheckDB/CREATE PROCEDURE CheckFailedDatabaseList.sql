USE [DBARestore]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*=========================================================
NAME:			dbo.CheckFailedDatabaseList
DESCRIPTION:	Reads the dbo.DatabaseRestoreHistory table and sends
				an email to the DBAs if the list of Server/Database 
				combinations which failed the Database Restore and 
				CHECKDB process since the previous Monday is greater
				than zero.
INPUTS:			None
OUTPUTS:		None
RETURN:			Recordset of Server/Database combinations
RESULTS:		None
ERRORS:			No Custom Errors
DEPENDENCIES:	dbo.DatabaseRestoreHistory
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20191120	Initial Author
									
=========================================================
--DEBUG--

EXEC dbo.CheckFailedDatabaseList
    
=========================================================*/
CREATE OR ALTER PROCEDURE [dbo].[CheckFailedDatabaseList]
AS
BEGIN
	-- determine date of previous Monday
	SET DATEFIRST 1	--Monday
	DECLARE @DoW INT
	DECLARE @CurrentDate DATE
	DECLARE @PrevMondayDate DATE

	SET @CurrentDate = GETDATE()
	SET @DoW = (SELECT DATEPART(dw, @CurrentDate))
	SET @PrevMondayDate = DATEADD(DAY, -@DoW+1, @CurrentDate)
	PRINT @PrevMondayDate


	-- I know it is ugly to create and drop a permanent table in the database, but I could
	-- not get the email query to work properly in a table variable or a temp table.
	BEGIN TRY DROP TABLE dbo.TestDatabases END TRY BEGIN CATCH END CATCH

	-- Create work table
	CREATE TABLE dbo.TestDatabases
	(
		SourceServer		VARCHAR(128),
		SourceDomain		VARCHAR(128),
		SourceInstance		VARCHAR(128),
		SourceDatabaseName	VARCHAR(128),
		EndRunDate			DATETIME,
		LastRunResult		VARCHAR(50)
	)

	-- Select all Server/Database combinations since the 
	-- last Monday with their most recent end date
	INSERT INTO dbo.TestDatabases
		SELECT
			SourceServer,
			SourceDomain,
			SourceInstance,
			SourceDatabaseName,
			MAX(EndRunDate),
			NULL
		FROM dbo.DatabaseRestoreHistory
		WHERE EndRunDate >= @PrevMondayDate
		GROUP BY SourceServer, SourceDomain, SourceInstance, SourceDatabaseName

	-- Update the work table with the result of each combination
	-- that matches their latest end date
	UPDATE dbo.TestDatabases SET
		LastRunResult = drh.LastRunResult
	FROM dbo.TestDatabases d
		INNER JOIN dbo.DatabaseRestoreHistory drh ON
			d.SourceServer = drh.SourceServer
			AND
			d.SourceDomain = drh.SourceDomain
			AND
			d.SourceInstance = drh.SourceInstance
			AND
			d.SourceDatabaseName = drh.SourceDatabaseName
			AND
			d.EndRunDate = drh.EndRunDate

	-- Delete any combinations that succeeded the last time they ran
	DELETE FROM dbo.TestDatabases WHERE LastRunResult = 'Success'

	IF EXISTS (SELECT 1 FROM dbo.TestDatabases)
	BEGIN
		EXEC msdb..sp_send_dbmail 
			@profile_name='DL.DatabaseAdministrators@PremiseHealth.com',
			@recipients='DL.DatabaseAdministrators@PremiseHealth.com',
			@subject='Automated Restore CheckDB Database Failures',
			@body='The server/database combinations in the attached file failed the database restore/checkdb process this week.',
			@query = 'SELECT SourceServer, SourceDomain, SourceInstance, SourceDatabaseName, CONVERT(VARCHAR(20), EndRunDate, 121), LastRunResult FROM dbo.TestDatabases',
			@execute_query_database = 'DBARestore',
			@attach_query_result_as_file = 1,
			@query_attachment_filename = 'Failed Database List.txt',
			@query_result_width = 1024;
	END

	-- get rid of the ugly permanent table
	BEGIN TRY DROP TABLE dbo.TestDatabases END TRY BEGIN CATCH END CATCH
END
GO


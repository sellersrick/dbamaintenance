USE [DBARestore]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

BEGIN TRY DROP TABLE dbo.DatabaseRestoreHistory END TRY BEGIN CATCH END CATCH

CREATE TABLE dbo.DatabaseRestoreHistory
(
	DatabaseRestoreHistoryID int IDENTITY(1,1) NOT NULL,
	SourceServer varchar(128) NOT NULL,
	SourceDomain varchar(128) NOT NULL,
	SourceInstance varchar(128) NOT NULL,
	SourceDatabaseName varchar(128) NOT NULL,
	BackupUNCPath varchar(1024) NOT NULL,
	StartRunDate datetime NULL,
	EndRunDate datetime NULL,
	LastRunResult varchar(50) NULL,
 CONSTRAINT PK_DatabaseRestoreHistory PRIMARY KEY CLUSTERED 
(
	DatabaseRestoreHistoryID ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO


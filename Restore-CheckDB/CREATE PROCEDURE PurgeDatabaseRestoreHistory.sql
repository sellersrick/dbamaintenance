/*=========================================================
NAME:			dbo.PurgeDatabaseRestoreHistory
DESCRIPTION:	Purges the dbo.DatabaseRestoreHistory table
				of records older then the specified number
				of days.  Default is 180 days.
INPUTS:			None
OUTPUTS:		None
RETURN:			None
RESULTS:		None
ERRORS:			No Custom Errors
DEPENDENCIES:	dbo.DatabaseRestoreHistory
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20191115	Initial Author
									
=========================================================
--DEBUG--

EXEC dbo.PurgeDatabaseRestoreHistory
    
=========================================================*/
CREATE or ALTER PROCEDURE dbo.PurgeDatabaseRestoreHistory
	@NbrOfDays		INT = 180
AS
BEGIN
	DELETE FROM dbo.DatabaseRestoreHistory
	WHERE EndRunDate < DATEADD(DAY, -@NbrOfDays, GETDATE())
END
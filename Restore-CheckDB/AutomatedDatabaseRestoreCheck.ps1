﻿#*=============================================================================
#* Script Name:  AutomatedDatabaseRestoreCheck.ps1
#*=============================================================================
#* Created:     11/25/2019
#* Author:      Rick Sellers
#* Company:     Premise Health
#
#*=============================================================================
# Description
#*=============================================================================
# Builds a list of servers to process based on the parameters specified. It then restores the 
# database backup for the database, runs a full CHECKDB on it, and logs the results.  If there 
# is an error returned from CHECKDB, an email is sent to the DBA team and the restored
# database and its associated CHECKDB messages table are left intact for analysis. 
#
# Due to possible space restrictions, an error described above will terminate the 
# run process.
#
#*=============================================================================
# Parameters
#*=============================================================================
#  WorkFolder [string] - specifies the folder to be used to temporarily hold the
#    database backup file to restore
#
#  TargetServerName [string] - Name of the server on which the restore/CHECKDB
#     process is to be performed
#
#  TargetWorkDatabase [string] - Specifies the name of the database which holds
#     the settings table and will be used to hold the CHECKDB messages table
#     for databases being processed.
#
#  LogFolderPath [string] - Specifies the path to which the log files are written.
#
#  FirstServerName [string] - Specifies the name of the first server to process 
#    in a restart or one-off situation
#
#  NumberOfServerToProcess [int] - Specifies the number of servers to process 
#    starting with the FirstServerName specified
#
#  BuildDatabaseListfromHistory [switch] - Specifies to build a list of 
#    server/database pairs to process from the history file where a failure was recorded
#
#*=============================================================================
# Example
#*=============================================================================
#  AutomatedDatabaseRestoreCheck.ps1 -WorkFolder 'B:\RestoreBackups' -TargetServerName 'P-DB-SQLDBA-02' -TargetWorkDatabase 'DBARestore' 
#
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date:        [DATE_MDY]
#* Time:        [TIME]
#* Issue:
#* Solution:
#*
#*=============================================================================

<#
.SYNOPSIS
    AutomatedDatabaseRestoreCheck.ps1 -WorkFolder 'B:\RestoreBackups' -TargetServerName 'P-DB-SQLDBA-02' -TargetWorkDatabase 'DBARestore' 

.DESCRIPTION
    Builds a list of servers to process based on the parameters specified. It then restores the 
    database backup for the database, runs a full CHECKDB on it, and logs the results.  If there 
    is an error returned from CHECKDB, an email is sent to the DBA team and the restored
    database and its associated CHECKDB messages table are left intact for analysis. 

.PARAMETER WorkFolder
    Specifies the folder to be used to temporarily hold the database backup file to restore

.PARAMETER TargetServerName
    Name of the server on which the restore/CHECKDB process is to be performed

.PARAMETER TargetWorkDatabase
    Specifies the name of the database which holds the settings table and will be used to hold 
    the CHECKDB messages table for databases being processed.

.PARAMETER LogFolderPath
    Specifies the local disk path to which log files are written.

.PARAMETER FirstServerName
    Specifies the name of the first server to process in a restort or one-off situation

.PARAMETER NumberOfServerToProcess
    Specifies the number of servers to process starting with the FirstServerName specified

.PARAMETER BuildDatabaseListfromHistory
    Specifies to build a list of server/database pairs to process from the history file where a failure was recorded

.OUTPUTS
    Text log file for each of the databases processed.
    Record in DBARestore.dbo.DatabaseRestoreHistory table on P-DB-SQLDBA-02 server

.EXAMPLE
    AutomatedDatabaseRestoreCheck.ps1 -WorkFolder 'B:\RestoreBackups' -TargetServerName 'P-DB-SQLDBA-02' -TargetWorkDatabase 'DBARestore'
    AutomatedDatabaseRestoreCheck.ps1 -WorkFolder 'B:\RestoreBackups' -TargetServerName 'P-DB-SQLDBA-02' -TargetWorkDatabase 'DBARestore' -FirstServerName DBWAREHOUSE -NumberOfServersToProcess 1
    AutomatedDatabaseRestoreCheck.ps1 -WorkFolder 'B:\RestoreBackups' -TargetServerName 'P-DB-SQLDBA-02' -TargetWorkDatabase 'DBARestore' -BuildDatabaseListFromHistory

#>


Param
(
    [Parameter(Mandatory=$false)]
    [string]$WorkFolder = 'B:\RestoreBackups',
    [Parameter(Mandatory=$false)]
    [string]$TargetServerName = 'P-DB-SQLDBA-02',
    [Parameter(Mandatory=$false)]
    [string]$TargetWorkDatabase = 'DBARestore',
    [Parameter(Mandatory=$false)]
    [string]$LogFolderPath = 'S:\RestoreLogs',
    [Parameter(Mandatory=$false)]
    [string]$FirstServerName,
    [Parameter(Mandatory=$false)]
    [int]$NumberOfServersToProcess = 99,
    [Parameter(Mandatory=$false)]
    [switch]$BuildDatabaseListFromHistory


)

import-module dbatools

try
{
    # Create a $Task variable to keep track of what we are doing.  Every step in the process has the -ErrorAction Stop -ErrorVariable ErrorDetails parameters.
    # If an error occurs, the code will go to the catch block, which will determine what to do based on the task that failed.  Valid tasks are:
    #
    # Get Server List
    # Get Database List
    # Drop old CHECKDB table
    # Drop old Test Database
    # Get Backup Information
    # Copy Backup
    # Restore Database
    # Delete backup file
    # Create CHECKDB table
    # Run CHECKDB
    # Drop CHECKDB Table
    # Drop Test Database


    [string]$Task = ''

    # DEBUG overrides for testing
    #[string]$WorkFolder = 'B:\RestoreBackups'
    #[string]$TargetServerName = 'P-DB-SQLDBA-02'
    #[string]$TargetWorkDatabase = 'DBARestore'
    #[string]$LogFolderPath = 'S:\RestoreLogs'

    [string]$Logfile = "$LogFolder\AutomatedDatabaseRestoreCheck.log-" + (Get-Date).ToString('yyyyMMdd') + '.log'

    # declare ServerInfo class
    class ServerInfo
    {
        [string]$ServerName
        [string]$DomainName
        [string]$InstanceName
    }

    # declare DatabaseInfo class
    class DatabaseInfo
    {
        [string]$ServerName
        [string]$DomainName
        [string]$InstanceName
        [string]$DatabaseName
        [string]$BackupUNCPath
        [datetime]$StartTime
        [datetime]$EndTime
        [string]$Result
    }

    # declare ServerList array
    [System.Collections.ArrayList]$ServerList = @()

    # if we get the override switch, we process it here
    if ($BuildDatabaseListFromHistory -eq $false)
    {
        [string]$SQL = "SELECT "

        if($null -ne $NumberOfServersToProcess)
        {
            $SQL += "TOP($NumberOfServersToProcess) "
        }

        if ($null -eq $FirstServerName -or $FirstServerName -eq '')
        {
            $SQL += " * FROM [dbo].[vwMasterServerList] WHERE (Tier = 1 OR ServerName = 'P-DB-OHMUAT-01') AND ServerName NOT IN ('P-DB-EHS-01', 'P-DB-SQLDBA-02', 'P-DB-NGARC-01') ORDER BY MasterServerListID"

        }
        else
        {
            $SQL += " * FROM [dbo].[vwMasterServerList] WHERE (Tier = 1 OR ServerName = 'P-DB-OHMUAT-01') AND ServerName NOT IN ('P-DB-EHS-01', 'P-DB-SQLDBA-02', 'P-DB-NGARC-01') AND MasterServerListID >= (SELECT MasterserverListID FROM vwMasterServerList WHERE ServerName = '$FirstServerName') ORDER BY MasterServerListID"
        }
    }
    else
    {
        # build a dummy server list for the server loop - the server in it will be ignored when building the database list 
        $SQL = "SELECT TOP(1) * FROM [dbo].[vwMasterServerList] WHERE (Tier = 1 OR ServerName = 'P-DB-OHMUAT-01') AND ServerName NOT IN ('P-DB-EHS-01', 'P-DB-SQLDBA-02', 'P-DB-NGARC-01') AND ServerName = 'DBWAREHOUSE' ORDER BY MasterServerListID"
    }

    Write-Host $SQL

    $Task = 'Get Server List'
    $Servers = Invoke-Sqlcmd -Query $SQL -ServerInstance C-DB-SQLDBA-01 -Database DBATools -ErrorAction Stop -ErrorVariable ErrorDetails

    # build ServerList array
    foreach ($Server in $Servers)
    {
        $ServerInfo = New-Object -TypeName ServerInfo
        $ServerInfo.ServerName = $Server.ServerName
        $ServerInfo.DomainName = $Server.Domain
        $ServerInfo.InstanceName = $Server.InstanceName

        $ServerList.Add($ServerInfo) | Out-Null
    }

    $ServerList | Format-List

    foreach ($Server in $ServerList)
    {
        #$Server | Format-List

        # declare DatabaseList array
        [System.Collections.ArrayList]$DatabaseList = @()

        # if we get the override switch, we process it here in the "else" block
        if ($BuildDatabaseListFromHistory -eq $false)
        {
            Write-Host $Server.ServerName

            $Task = 'Get Database List'
            [string]$SQL = "SELECT [name] FROM sys.databases WHERE database_id BETWEEN 5 AND 32766 AND state_desc = 'ONLINE'"
            $DBs = Invoke-Sqlcmd -query $SQL -ServerInstance $Server.ServerName -Database master -ErrorAction Stop -ErrorVariable ErrorDetails
            # build DatabaseList array
            foreach ($DB in $DBs)
            {
                $DatabaseInfo = New-Object -TypeName DatabaseInfo
                $DatabaseInfo.ServerName = $Server.ServerName
                $DatabaseInfo.DomainName = $Server.DomainName
                $DatabaseInfo.InstanceName = $Server.InstanceName
                $DatabaseInfo.DatabaseName = $DB.name
                $DatabaseInfo.BackupUNCPath = ''
                $DatabaseInfo.Result = ''

                $DatabaseList.Add($DatabaseInfo) | Out-Null
            }
        }
        else
        {
            $Task = 'Get Database List'
            [string]$SQL = "EXEC dbo.BuildFailedDatabaseList"
            $SQL | Out-File -LiteralPath $LogFile -Append
            $DBs = Invoke-Sqlcmd -query $SQL -ServerInstance $TargetServerName -Database $TargetWorkDatabase -ErrorAction Stop -ErrorVariable ErrorDetails
            
            # build DatabaseList array
            foreach ($DB in $DBs)
            {
                $DatabaseInfo = New-Object -TypeName DatabaseInfo
                $DatabaseInfo.ServerName = $DB.SourceServer
                $DatabaseInfo.DomainName = $DB.SourceDomain
                $DatabaseInfo.InstanceName = $DB.SourceInstance
                $DatabaseInfo.DatabaseName = $DB.SourceDatabaseName
                $DatabaseInfo.BackupUNCPath = ''
                $DatabaseInfo.Result = ''

                $DatabaseList.Add($DatabaseInfo) | Out-Null
            }
        }

        #$DatabaseList | Format-List

        foreach ($DatabaseInfo in $DatabaseList)
        {
            try
            {
                # try to suspend operations from 2:30 - 5:00 AM to avoid most backup times
                $Time = (Get-Date).ToString('HH:mm:ss')
                if ($Time -ge '02:30:00' -and $Time -le '05:00:00')
                {
                    # sleep for 2.5 hours
                    Start-Sleep -s 9000
                }

                # print current database name to output
                Write-Host $DatabaseInfo.DatabaseName

                [boolean]$FatalError = $false

                # set value of $Result to Success...assuming it will be succcessful.  Any error will change this value.
                [string]$DatabaseInfo.Result = 'Success'
                [string]$SourceServerName = $DatabaseInfo.ServerName
                [string]$SourceInstanceName = $DatabaseInfo.InstanceName
                [string]$SourceDatabaseName = $DatabaseInfo.DatabaseName
                [string]$LogFolder = $LogFolderPath
                [string]$LogFile = "$LogFolder\Restore-$SourceServerName[$SourceInstanceName].$SourceDatabaseName-" + (Get-Date).ToString('yyyyMMdd') + '.log'
                [string]$DatabaseName = "Test_$SourceDatabaseName"
                [string]$BackupFilePath = ''

                # Start log file for this database
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Start processing database $DatabaseName from $SourceServerName\$SourceInstanceName" | Out-File -LiteralPath $LogFile -ErrorAction Stop -ErrorVariable ErrorDetails

                # preserve start time in current database object
                $DatabaseInfo.StartTime = $Now


                # Static SQL to create table to hold DBCC CHECKDB messages
                # It has to be done this way because the table name changes for every database
                [string]$SQLCreateTable = 
                "USE $TargetWorkDatabase;
                CREATE TABLE dbo.[DBCC_$DatabaseName]
                (
                    [Error] int NULL,
                    [Level] int NULL,
                    [State] int NULL,
                    MessageText varchar(7000) NULL,
                    RepairLevel int NULL,
                    [Status] int NULL,
                    [DbId] int NULL,
                    DbFragId int NULL,
                    ObjectId int NULL,
                    IndexId int NULL,
                    PartitionID int NULL,
                    AllocUnitID int NULL,
                    RidDbId int NULL,
                    RidPruId int NULL,
                    [File] int NULL,
                    [Page] INT NULL,
                    Slot int NULL,
                    RefDbId int NULL,
                    RefPruId int NULL,
                    RefFile int NULL,
                    RefPage int NULL,
                    RefSlot int NULL,
                    Allocation int NULL
                )"


                # clean up leftover CHECKDB table if it exists
                $Task = "Drop old CheckDB Table"
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - clean up leftover CHECKDB table if it exists" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                Invoke-SqlCmd -Query "IF EXISTS(SELECT 1 FROM sys.tables WHERE [name] = 'DBCC_$DatabaseName' AND [type] = 'U') DROP TABLE [DBCC_$DatabaseName]" -ServerInstance $TargetServerName -Database $TargetWorkDatabase -QueryTimeout 600 -ErrorAction Stop -ErrorVariable ErrorDetails

                # clean up leftover database if it exists
                $Task = "Drop old Test Database"
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Clean up leftover database if it exists" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                Invoke-SqlCmd -Query "IF EXISTS(SELECT 1 FROM sys.databases WHERE [name] = '$DatabaseName') DROP DATABASE [$DatabaseName]" -ServerInstance $TargetServerName -Database 'master' -QueryTimeout 600 -ErrorAction Stop -ErrorVariable ErrorDetails

                # Convert the local path to UNC format
                if ($SourceInstanceName -eq 'MSSQLSERVER')
                {
                    $BackupFilePath = "\\$SourceServerName\Backups"
                }
                else
                {
                    # if it is a named instance, the share must be named <servername>\Backups<instancename>, e.g. P-DB-SQLDBA-01\BackupsPOC
                    $BackupFilePath = "\\$SourceServerName\Backups$SourceInstanceName"
                }

                # Record UNC path in the log file
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - UNC Path = $BackupFilePath" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                # Get the full backup information for the source database - the IgnoreLogBackup switch ensures this
                $Task = "Get Backup Information"
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Getting database backup file information for Database $SourceDatabaseName" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                $BI = Get-DbaBackupInformation -SqlInstance $SourceServerName\$SourceInstanceName -Path $BackupFilePath -Database $SourceDatabaseName -MaintenanceSolution -IgnoreLogBackup -EnableException -ErrorAction Stop -ErrorVariable ErrorDetails

                # Check to make sure we could find a backup file.  If not, log the status and loop.
                # Note that the finally block will still execute
                if ($null -eq $BI)
                {
                    $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                    "$Now - No database backup file found for Database $SourceDatabaseName" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                    $DatabaseInfo.Result = 'No Backup File Found'

                    continue
                }

                # Select the last full backup - sort list descending and get the top 1
                $FullBackupObject = $BI | Sort-Object -Descending -Property 'End' | Select-Object SqlInstance, Database, 'End', Path -First 1 

                # Get the full path for the backup file
                $FullPath = ($FullBackupObject.Path)[0]

                # Record Full Path in the log file
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Full Path = $FullPath" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
        
                # store the backup path in the DatabaseInfo object
                $DatabaseInfo.BackupUNCPath = $FullPath

                # print the DatabaseInfo object to the log file
                $DatabaseInfo | Format-List | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                # Extract just the backup file name
                $BackupFileName = $FullPath | Split-Path -Leaf

                # Record Backup File Name to the log file
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Backup File Name = $BackupFileName" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                # Record Copy Command to the log file
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Copy Command = Copy-Item -Path $FullPath -Destination $WorkFolder" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                # Copy the backup file to the local work folder
                $Task = "Copy Backup"
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Copying database backup file to local folder $WorkFolder" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                Copy-Item -Path $FullPath -Destination $WorkFolder -ErrorAction Stop -ErrorVariable ErrorDetails

                # Record Restore Command to the log file
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Restore Command = Restore-DbaDatabase -SqlInstance $TargetServerName -DatabaseName ""$DatabaseName"" -Path ""$WorkFolder\$BackupFileName"" -ReplaceDbNameInFile -EnableException" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                # Issue the command to restore the database locally
                $Task = "Restore Database"
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Starting restore of database $Databasename" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                $DBRestore = Restore-DbaDatabase -SqlInstance $TargetServerName -DatabaseName $DatabaseName -Path "$WorkFolder\$BackupFileName" -ReplaceDbNameInFile -EnableException -ErrorAction Stop -ErrorVariable ErrorDetails | Out-File -LiteralPath $LogFile -Append
                $DBRestore | Format-List | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                if ($DBRestore.RestoreComplete -eq $false)
                {
                    throw
                }

                # Update Log File
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Database $DatabaseName from $SourceServerName\$SourceInstanceName restored successfully" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                # Record Delete Backup Command to the log file
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Delete Backup Command = Remove-Item -Path ""$WorkFolder\$BackupFileName""" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                # Delete the backup file
                $Task = "Delete backup file"
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Deleting backup file $WorkFolder\$BackupFileName" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                Remove-Item -Path "$WorkFolder\$BackupFileName" -ErrorAction Stop -ErrorVariable ErrorDetails

                # Create the table to hold DBCC CHECKDB messages for this database using static SQL defined earlier
                $Task = "Create CHECKDB table"
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - Creating DBCC CHECKDB messages table" | Out-File -LiteralPath $LogFile -Append
                Invoke-SqlCmd -Query $SQLCreateTable -ServerInstance $TargetServerName -Database $TargetWorkDatabase -QueryTimeout 600 -ErrorAction Stop -ErrorVariable ErrorDetails

                # Execute the DBCC CHECKDB
                $Task = "Run CHECKDB"
                "$Now - Executing DBCC CHECKDB" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                [string]$SQL = "Use $TargetWorkDatabase; INSERT INTO dbo.[DBCC_$DatabaseName] EXEC ('DBCC CHECKDB (''$DatabaseName'') WITH TABLERESULTS, EXTENDED_LOGICAL_CHECKS, NO_INFOMSGS')"
                Invoke-SqlCmd -Query $SQL -ServerInstance $TargetServerName -Database "$TargetWorkDatabase" -QueryTimeout 0 -ErrorAction Stop -ErrorVariable ErrorDetails

                # Check to see if we got any errors - NO_INFOMSGS parameter will leave an empty messages table if no errors were found
                [System.Data.DataRow]$DataRow = Invoke-Sqlcmd -Query "USE $TargetWorkDatabase; SELECT COUNT(1) FROM dbo.[DBCC_$DatabaseName]" -ServerInstance $TargetServerName -Database "$TargetWorkDatabase" -ErrorAction Stop -ErrorVariable ErrorDetails
                [int]$Count = $DataRow[0]
                if ($Count -gt 0)
                {
                    # Override the result string
                    $DatabaseInfo.Result = 'DBCC CHECKDB Failure'

                    # Write the error messages to the log file
                    $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                    "$Now - DBCC CHECKDB Error Messages:" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                    Invoke-SqlCmd -Query "USE $TargetWorkDatabase; SELECT * FROM DBCC_$DatabaseName" -ServerInstance $TargetServerName -Database "$TargetWorkDatabase" -QueryTimeout 600 -ErrorAction Stop -ErrorVariable ErrorDetails | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                    # Log result of DBCC CHECKDB
                    $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                    "$Now - Result of DBCC CHECKDB is " + $DatabaseInfo.Result | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                    # get out if CHECKDB failed
                    throw 'DBCC CHECKDB Failure'
                }
                else
                {
                    $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                    "$Now - No errors found in database!" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                    # Log result of DBCC CHECKDB to the log file
                    $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                    "$Now - Result of DBCC CHECKDB is " + $DatabaseInfo.Result | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                    # Drop the messages table for DBCC CHECKDB
                    $Task = "Drop CHECKDB Table"
                    $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                    "$Now - Dropping DBCC CHECKDB messages table" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                    Invoke-SqlCmd -Query "USE $TargetWorkDatabase; DROP TABLE dbo.[DBCC_$DatabaseName]" -ServerInstance $TargetServerName -Database $TargetWorkDatabase -QueryTimeout 600 -ErrorAction Stop -ErrorVariable ErrorDetails

                    # Drop the database
                    $Task = "Drop Test Database"
                    $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                    "$Now - Dropping database $DatabaseName" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                    Invoke-SqlCmd -Query "USE [master]; ALTER DATABASE [$DatabaseName] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE; DROP DATABASE [$DatabaseName]" -ServerInstance $TargetServerName -Database $TargetWorkDatabase -QueryTimeout 600 -ErrorAction Stop -ErrorVariable ErrorDetails
                }

            }
            catch
            {
                # Drop old Test Database
                # Get Backup Information
                # Copy Backup
                # Restore Database
                # Delete backup file
                # Create CHECKDB table
                # Run CHECKDB
                # Drop CHECKDB Table
                # Drop Test Database

                switch ($Task)
                {
                    'Run CHECKDB' 
                    {
                        if ($DatabaseInfo.Result -ne 'DBCC CHECKDB Failure')
                        {
                            # if we have an exception, dump it to the log file
                            if ($null -ne $ErrorDetails)
                            {
                                "From catch block:" | Out-File -LiteralPath $LogFile -Append
                                foreach ($Err in $ErrorDetails)
                                {
                                    $err | Format-List * -Force | Out-File -LiteralPath $LogFile -Append
                                }
                            }

                            Write-Host $_.Exception | Format-List

                            # Write the exception message to the log file
                            "Processing Error Occured:" | Out-File -LiteralPath $LogFile -Append
                            $_.Exception | Format-List | Out-File -LiteralPath $LogFile -Append

                            # Send email alert for failed process
                            Send-MailMessage -From "$TargetServerName@PremiseHealth.com" -To DL.DatabaseAdministrators@Premisehealth.com -Subject "Database restore check for $SourceServerName\$SourceInstanceName.$SourceDatabaseName failed" -Body "Error Message:`n $_ `n See log file for details" -SmtpServer relay.premisehealth.com

                            break
                        }
                        else
                        {
                            $FatalError = $true

                            # Send email alert for failed process
                            Send-MailMessage -From "$TargetServerName@PremiseHealth.com" -To DL.DatabaseAdministrators@Premisehealth.com -Subject "Database restore check for $SourceServerName\$SourceInstanceName.$SourceDatabaseName failed" -Body "Errors detected when running CHECKDB.`nError Message:`n $_ `n See log file for details" -SmtpServer relay.premisehealth.com

                            break
                        }
                    }
                    'Restore Database'
                    {
                        $FatalError = $true

                        # Send email alert for failed process
                        Send-MailMessage -From "$TargetServerName@PremiseHealth.com" -To DL.DatabaseAdministrators@Premisehealth.com -Subject "Database restore for $SourceServerName\$SourceInstanceName.$SourceDatabaseName failed" -Body "Errors detected when running CHECKDB.`nError Message:`n $_ `n See log file for details" -SmtpServer relay.premisehealth.com

                        break
                    }
                    'Drop Test Database'
                    {
                        $FatalError = $true

                        # Send email alert for failed process
                        Send-MailMessage -From "$TargetServerName@PremiseHealth.com" -To DL.DatabaseAdministrators@Premisehealth.com -Subject "Database drop for $SourceServerName\$SourceInstanceName.$SourceDatabaseName failed" -Body "Errors detected when running CHECKDB.`nError Message:`n $_ `n See log file for details" -SmtpServer relay.premisehealth.com

                        break
                    }
                    default
                    {
                        $DatabaseInfo.Result = $Task

                        # if we have an exception, dump it to the log file
                        if ($null -ne $ErrorDetails)
                        {
                            "From catch block:" | Out-File -LiteralPath $LogFile -Append
                            foreach ($Err in $ErrorDetails)
                            {
                                $err | Format-List * -Force | Out-File -LiteralPath $LogFile -Append
                            }
                        }

                        Write-Host $_.Exception | Format-List

                        # Write the exception message to the log file
                        "Processing Error Occured:" | Out-File -LiteralPath $LogFile -Append
                        $_.Exception | Format-List | Out-File -LiteralPath $LogFile -Append

                        continue
                    }
                }

                # get out because we had a fatal error
                if ($FatalError -eq $true)
                {
                    break
                }
            }
            finally
            {
                # NOTE - If we had an error, we are leaving the test restore database and the DBCC CHECKDB messages table for it intact to aid in troubleshooting

                # store the end time in the DatabaseInfo object
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                $DatabaseInfo.EndTime = $Now

                # Insert record into the history table 
                $Task = 'Update History Table'
                "$Now - Updating the Settings table for database $SourceDatabaseName from $SourceServerName\$SourceInstanceName" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                $SQL = "INSERT INTO dbo.DatabaseRestoreHistory (SourceServer, SourceDomain, SourceInstance, SourceDatabaseName, BackupUNCPath, StartRunDate, EndRunDate, LastRunResult) VALUES( '" + $DatabaseInfo.ServerName + "', '" + $DatabaseInfo.DomainName + "', '" + $DatabaseInfo.InstanceName + "', '" + $DatabaseInfo.DatabaseName + "', '" + $DatabaseInfo.BackupUNCPath + "', '" + $DatabaseInfo.StartTime + "', '" + $DatabaseInfo.EndTime + "', '" + $DatabaseInfo.Result + "')"
                "$Now - INSERT Command = $SQL"  | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails
                Invoke-SqlCmd -Query $SQL -ServerInstance $TargetServerName -Database $TargetWorkDatabase -QueryTimeout 600 -ErrorAction Stop -ErrorVariable ErrorDetails

                # Close out the log file 
                $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
                "$Now - End processing database $SourceDatabaseName from $SourceServerName\$SourceInstanceName" | Out-File -LiteralPath $LogFile -Append -ErrorAction Stop -ErrorVariable ErrorDetails

                # Reset the logfile name back to the default
                $Logfile = "$LogFolder\AutomatedDatabaseRestoreCheck.log-" + (Get-Date).ToString('yyyyMMdd') + '.log'
            }
        }
    }       
}
catch
{
    # Get Server List
    # Get Database List

    switch ($Task)
    {
        'Get Server list'
        {
            $FatalError = $true

            Write-Host $_.Exception | Format-List

            # if we have an exception, dump it to the log file
            if ($null -ne $ErrorDetails)
            {
                "From catch block:" | Out-File -LiteralPath $LogFile -Append
                foreach ($Err in $ErrorDetails)
                {
                    $err | Format-List * -Force | Out-File -LiteralPath $LogFile -Append
                }
            }

            # Write the exception message to the log file
            "Processing Error Occured:" | Out-File -LiteralPath $LogFile -Append
            $_.Exception | Format-List| Out-File -LiteralPath $LogFile -Append

            # Send email alert for failed process
            Send-MailMessage -From "$TargetServerName@PremiseHealth.com" -To DL.DatabaseAdministrators@Premisehealth.com -Subject "Failed getting server list from C-DB-SQLDBA-01" -Body "Unable to get Server List.`nError Message:`n $_ `n See log file for details" -SmtpServer relay.premisehealth.com

            break
        }

        'Get DatabaseList list'
        {
            $FatalError = $true

            Write-Host $_.Exception | Format-List

            # if we have an exception, dump it to the log file
            if ($null -ne $ErrorDetails)
            {
                "From catch block:" | Out-File -LiteralPath $LogFile -Append
                foreach ($Err in $ErrorDetails)
                {
                    $err | Format-List * -Force | Out-File -LiteralPath $LogFile -Append
                }
            }

            # Write the exception message to the log file
            "Processing Error Occured:" | Out-File -LiteralPath $LogFile -Append
            $_.Exception | Format-List | Out-File -LiteralPath $LogFile -Append

            # Send email alert for failed process
            Send-MailMessage -From "$TargetServerName@PremiseHealth.com" -To DL.DatabaseAdministrators@Premisehealth.com -Subject "Failed getting database list for  $SourceServerName\$SourceInstanceName" -Body "Unable to get Database List.`nError Message:`n $_ `n See log file for details" -SmtpServer relay.premisehealth.com

            break
        }

        default
        {
            # dump current time to logfile
             $Now = (Get-Date).ToString('MM/dd/yyyy HH:mm:ss')
            "$Now - Miscellaneous error occurred." | Out-File -LiteralPath $LogFile -Append

           # record the current task if we have a $Databaseinfo object
            if ($null -ne $DatabaseInfo)
            {
                $DatabaseInfo.Result = $Task
                $DatabaseInfo | Format-List | Out-File -LiteralPath $LogFile -Append
            }


            # if we have an exception, dump it to the log file
            if ($null -ne $ErrorDetails)
            {
                "From catch block:" | Out-File -LiteralPath $LogFile -Append
                foreach ($Err in $ErrorDetails)
                {
                    $err | Format-List * -Force | Out-File -LiteralPath $LogFile -Append
                }
            }

            Write-Host $_.Exception | Format-List

            # if we have an exception, dump it to the log file
            # Write the exception to the log file
            "Processing Error Occured:" | Out-File -LiteralPath $LogFile -Append
            $_.Exception | Format-List | Out-File -LiteralPath $LogFile -Append

            # Send email alert for failed process
            Send-MailMessage -From "$TargetServerName@PremiseHealth.com" -To DL.DatabaseAdministrators@Premisehealth.com -Subject "Automated database backup/restore/checkdb process failed." -Body "Miscellaneous error.`nError Message:`n $_ `n See log file for details" -SmtpServer relay.premisehealth.com
        }
    }
    
    # get out because of major or unknown error
    break
}





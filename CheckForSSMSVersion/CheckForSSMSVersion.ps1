#*=============================================================================
#* Script Name:  CheckForSSMSVersion.ps1
#*=============================================================================
#* Created:     04/17/2020
#* Author:      Rick Sellers
#* Company:     Premise Health
#
#*=============================================================================
# Description
#*=============================================================================
# Scans a list of servers to determine whether or not SSMS v18 is installed
#
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date:
#* Author:
#* Change:
#*
#*============================================================================= 


<#
.SYNOPSIS
    ./CheckForSSMSVersion.ps1
    ./CheckForSSMSVersion.ps1 -Domain 'qua-prem-health.com'

.DESCRIPTION
    Scans a list of servers to determine whether or not SSMS v18 is installed

.PARAMETER MSLServer
    FQDN of SQL Server hosting the MasterServerList table

.PARAMETER MSLUser
    Name of the user to read the server list from the MSLServer

.PARAMETER KeyFilePath
    Path to the key file for the SecureString encrypted password file.

.PARAMETER PswdFilePath
    Path to the encrypted password file for the MSLUser

.PARAMETER Domain
    Specifies the domain for which to run the scan

.OUTPUTS
    Writes to the output a list of the servers scanned and a boolean value indicating whether SSMS v18 is installed

.EXAMPLE
    ./CheckForSSMSVersion.ps1
    ./CheckForSSMSVersion.ps1 -Domain 'qua-prem-health.com'
#>


param
(
    [Parameter(Mandatory=$false)]
    [string]$MSLServer = 'C-DB-SQLDBA-01.premisehealth.com',
    [Parameter(Mandatory=$false)]
    [string]$MSLUser = 'DBAMaintDeployment',
    [Parameter(Mandatory=$false)]
    [string]$KeyFilePath = '.\KeyFile.txt',
    [Parameter(Mandatory=$false)]
    [string]$PswdFilePath = '.\PasswordFile.txt',
    [Parameter(Mandatory=$false)]
    [string]$Domain = 'premisehealth.com'


)

begin
{
    # ensure that we explicitly import module sqlserver or sqlps module will be loaded instead, resulting in errors
    import-module sqlserver
    Import-Module Security
}

process
{
    # define class to hold results
    class ServerCheckResults
    {
        [string] $ServerName
        [boolean] $Result

        # default constructor must be specified since we are defining a specific constructor
        ServerCheckResults(){}

        # specific constructor used to instantiate and populate the class object all at once
        ServerCheckResults([string] $Servername, [boolean] $Result)
        {
            $this.ServerName = $ServerName
            $this.Result = $Result
        }
    }


    try
    {
        [boolean]$IsPathExist = $false
        $Results = [System.Collections.ArrayList]@()

        #Create a new PSCredential object
        $PSCredential = New-PSCredential -User $MSLUser -SSPswdFile $PswdFilePath -EncryptionKeyFile $KeyFilePath -Logger $null;

        # define SQL query string using Domain parameter
        [string] $sql = "SELECT ServerName, Domain FROM DBATools.dbo.MSL WHERE Domain IN ('$Domain')"

        # build sorted list of DataRow objects containing the servers to process
        $Servers = Invoke-Sqlcmd -ServerInstance $mslserver -Credential $PSCredential -Query $sql -ErrorAction Stop -ErrorVariable ErrorVar -OutputAs DataRows | Sort-Object -Property 'ServerName'

        # loop through the servers list
        foreach ($Server in $Servers)
        {
            $IsPathExist = $false

            # build FQDN server name
            $ServerName = $Server["ServerName"] + "." + $Server["Domain"]

            # invoke command on remote server to check for the SSMS v18 path
            $IsPathExist = Invoke-Command -ComputerName $ServerName -ErrorAction Stop -ErrorVariable ErrorVar -Scriptblock {
                $IsPathExist = Test-Path 'S:\Program Files (x86)\Microsoft SQL Server Management Studio 18'
                $IsPathExist
            }

            # Build new ServerCheckResults instance calling the specific constructor we defined earlier
            $Data = [ServerCheckResults]::new($Server["ServerName"], $IsPathExist)

            # add the new instance to the results arraylist
            $Results.Add($Data) | Out-Null
        }

        # output the results arraylist
        $Results

    }
    catch
    {
        # Dump both the $_ and the $Errorvar values to the output file
        Write-Output $_
        Write-Output $ErrorVar
    }
    finally
    {
        # code in this block will always be executed, even if an error causes
        # execution to enter the catch{} block
    }
}

end
{
    # Shutdown and/or cleanup code will be placed here if needed.
    # This includes making sure the Logging module, if used, is shut down properly.
}




<#
B:\SSMS\SSMS v18.4 Setup ENU.exe /quiet /install /norestart /log log.txt
#>
USE [SolarWindsOrion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[sp_PH_rpt_GetMetricValues]
	@ServerTier			VARCHAR(3) = 'ALL',
	@ServerName			VARCHAR(256) = NULL,
	@ApplicationName	VARCHAR(256) = NULL,
	@ComponentName		VARCHAR(256) = NULL,
	@AggregationLevel	VARCHAR(10) = 'Minute',
	@StartDate			DATETIME = NULL,
	@StopDate			DATETIME = NULL,
	@Comparison			CHAR(2) = NULL,
	@WarningLevel		INT = 300,
	@CriticalLevel		INT = 100,
	@IncludeAlarmOnly	BIT = 0

/*=========================================================
NAME:			dbo.sp_PH_rpt_GetMetricValues
DESCRIPTION:	Retrieves the collected metric values from the 
				SolarWindsOrion repository database for the
				specified parameters.
INPUTS:			@ServerTier - the tier of the server(s) for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					server(s) to which this application is attached.
				@ServerName - the name of the server for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					server to which this application is attached.
				@ApplicationName - the name of the application 
					template set up in Orion and attached to
					a server.
				@ComponentName - the name of a component within
					the application that specifies the metric
					being collected.
				@AggregationLevel - the level to which aggregation
					done in the returned dataset.  valid values are:
						Minute - the lowest level at which metric
							collection occurs.  This is normally
							in 60-second intervals,
						Hourly - Aggregated to the average for each
							hour.
						Daily - aggregated to the average for each day
					Lowest-level aggregates are available for 7 days,
						then aggregated to hourly level for 30 days, 
						then aggregated to daily levels for one year.
						These are the defaults for Orion and have been
						left at these settings.  If the aggregation 
						level specified is not valid for the date range
						specified, it is promoted automatically to the
						lowest aggregation level that is valid.
				@StartDate - the starting date and time for the results
				@StopTime - the ending date and time for the results
				@WarningLevel - PLE value below which a warning condition is raised
				@CriticalLevel - PLE value below which a critical condition is raised
				@IncludeAlarmOnly - bit flag to control whether only alarm condition servers are returned
OUTPUTS:		None
RETURN:			None
RESULTS:		Resultset of the metric values for the specified parameters
ERRORS:			50000 - invalid server name
				50001 - application name cannot be NULL
				50002 - invalid application name
				50003 - application name not active for specified server
				50004 - invalid component name
				50005 - component name not active for specified server
				50006 - invalid aggregation level specified
				50007 - @StartDate is not a valid date
				50008 - @StopDate is not a valid date
				50009 - invalid date range specified
DEPENDENCIES:	dbo.APM_Application
				dbo.APM.Component
				dbo.APM_ComponentStatus_Daily
				dbo.APM_ComponentStatus_Detail
				dbo.APM_ComponentStatus_Hourly
				dbo.APM_PortEvidence (Solarwinds view)
				dbo.NodesCustomProperties
				dbo.NodesData
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20171229	Initial Author
  rick.sellers@premisehealth.com	20180110	Added warning and critical levels
  rick.sellers@premisehealth.com	20180417	Changed to capture ALL database servers
									
=========================================================
--DEBUG--

DECLARE @ServerTier				VARCHAR(3) = '3';
DECLARE @ServerName				VARCHAR(256) = 'ALL';
DECLARE @ApplicationName		VARCHAR(256) = '_DBA_Metrics_Collection - 1 Minute';
DECLARE @ComponentName			VARCHAR(256) = 'PLE';
DECLARE @AggregationLevel		VARCHAR(10) =  'Minute';
DECLARE @StartDate				DATETIME = '2018-01-09 06:00';
DECLARE @StopDate				DATETIME = '2018-01-10 06:00';
DECLARE @Comparison				CHAR(2) = '<=';
DECLARE @WarningLevel			INT = 1000;
DECLARE @CriticalLevel			INT = 300;
DECLARE @IncludeAlarmOnly		BIT = 0;

EXEC dbo.sp_PH_rpt_GetMetricValues @ServerTier, @ServerName, @ApplicationName, @ComponentName, @AggregationLevel, @StartDate, @StopDate, @Comparison, @WarningLevel, @CriticalLevel, @IncludeAlarmOnly
    
=========================================================*/
AS
BEGIN
	--*************************************************************************************************
	--  Verify ServerName and that it is a SQL Server
	--*************************************************************************************************
	IF (@ServerName IS NULL) SET @ServerName = 'ALL';

	IF NOT EXISTS (SELECT 1 
					FROM dbo.NodesData n
						INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
					WHERE n.Caption = @ServerName OR @ServerName = 'ALL'
						AND ncp.ServerType LIKE '%SQL%'
				  )
	BEGIN
		;THROW 50000, 'Specified @ServerName was not found.', 1;
	END

	--*************************************************************************************************
	--  Verify ApplicationName
	--*************************************************************************************************
	IF (@ApplicationName IS NULL)
	BEGIN
		;THROW 50001, 'Parameter @ApplicationName cannot be NULL.', 1;
	END

	IF NOT EXISTS (SELECT 1
					FROM dbo.APM_Application a
					WHERE a.[Name] = @ApplicationName
				  )
	BEGIN
		;THROW 50002, 'Specified @ApplicationName does not exist.', 1;
	END

	IF NOT EXISTS (SELECT 1
					FROM dbo.APM_Application a
						INNER JOIN dbo.NodesData n ON n.NodeID = a.NodeID
						INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
					WHERE a.[Name] = @ApplicationName
						AND ((n.Caption = @ServerName OR @ServerName = 'ALL')
							AND ncp.ServerType LIKE '%SQL%'))
	BEGIN
		;THROW 50003, 'Specified @ApplicationName is not active for the specified server.', 1;
	END

	--*************************************************************************************************
	--  Verify ComponentName
	--*************************************************************************************************
	IF NOT EXISTS (SELECT 1
					FROM dbo.APM_Component c
					WHERE c.[Name] = @ComponentName
				  )
	BEGIN
		;THROW 50004, 'Specified @ComponentName does not exist.', 1;
	END

	IF NOT EXISTS (SELECT 1
					FROM dbo.APM_Component c 
						INNER JOIN dbo.APM_Application a ON c.ApplicationID = a.ID
						INNER JOIN dbo.NodesData n ON n.NodeID = a.NodeID
						INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
					WHERE a.[Name] = @ApplicationName
						AND ((n.Caption = @ServerName OR @ServerName = 'ALL')
							AND ncp.ServerType IN ('App/SQL', 'Citrix/SQL', 'SQL'))
				  )
	BEGIN
		;THROW 50005, 'Specified @ComponentName is not active for the specified server.', 1;
	END

	--*************************************************************************************************
	--  Verify Aggregation Level
	--*************************************************************************************************
	IF (@AggregationLevel NOT IN ('Daily', 'Hourly', 'Minute'))
	BEGIN
		;THROW 50006, 'AggregationLevel must be ''Daily'', ''Hourly'', or ''Minute''.', 1;
	END

	-- If more than one hour time difference or StopDate more than 7 days ago, change the
	--  aggregation level to hourly, but DO NOT override a Daily aggregation level
	IF ((DATEDIFF(MINUTE, @StartDate, @StopDate) > 60 OR DATEDIFF(DAY, @StopDate, GETDATE()) > 7) AND @AggregationLevel <> 'Daily')
	BEGIN
		SET @AggregationLevel = 'Hourly';
	END

	-- If more than 13 days time difference or stop date > 30 days ago, change the
	--  aggregation level to daily
	IF (DATEDIFF(DAY, @StartDate, @StopDate) >= 14 OR DATEDIFF(DAY, @StopDate, GETDATE()) > 30)
	BEGIN
		SET @AggregationLevel = 'Daily';
	END

	--*************************************************************************************************
	--  Verify date range
	--*************************************************************************************************
	IF (ISDATE(@StartDate)) <> 1
	BEGIN
		;THROW 50007, '@StartDate value is not a valid date.', 1;
	END

	IF (ISDATE(@StopDate)) <> 1
	BEGIN
		;THROW 50008, '@StopDate value is not a valid date.', 1;
	END

	IF (@StopDate <= @StartDate)
	BEGIN
		;THROW 50009, '@StopDate must be >= @StartDate.', 1;
	END

	--*************************************************************************************************
	--  Convert dates to DATETIMEOFFSET type for Central Time Zone to pull correct data
	--*************************************************************************************************
	DECLARE @StartDateUTC DATETIMEOFFSET = @StartDate AT TIME ZONE 'Central Standard Time';
	DECLARE @StopDateUTC DATETIMEOFFSET = @StopDate AT TIME ZONE 'Central Standard Time';

	--*************************************************************************************************
	--  Declare table variables (table variables are used because these should never exceed 1,000 rows.
	--  If this becomes an issue, these should be changed to temp tables
	--*************************************************************************************************
	DECLARE @ServerNames TABLE
	(
		ServerNamesID		INT IDENTITY(1,1),
		NodeID				INT,
		ServerName			VARCHAR(255)
	);

	DECLARE @AppIDs TABLE
	(
		AppID		INT,
		AppName		VARCHAR(1000),
		NodeName	VARCHAR(256)
	);

	DECLARE @CompIDs TABLE
	(
		CompID		INT,
		CompName	VARCHAR(1000),
		AppID		INT
	);


	--*************************************************************************************************
	--  Declare a temp table to hold the ComponentStatusID key values. These are the keys to the actual
	--  tables that hold the metric values. 
	--*************************************************************************************************
	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#ComponentStatusIDs'))
	BEGIN
		DROP TABLE #ComponentStatusIDs;
	END

	CREATE TABLE #ComponentStatusIDs
	(
		ComponentStatusID	BIGINT,
		ApplicationID		INT,
		ComponentID			INT,
		[TimeStamp]			DATETIMEOFFSET(1)
	);

	----*************************************************************************************************
	----  Declare a temp table to hold the raw data values.  Note that we have five
	----   computed columns that automatically split out the date parts so that we do not have to run 
	----   functions in the SELECT or GROUP BY clauses
	----*************************************************************************************************
	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#RawValues'))
	BEGIN
		DROP TABLE #RawValues;
	END

	CREATE TABLE #RawValues
	(
		ServerName			VARCHAR(255),
		ApplicationName		VARCHAR(255),
		ComponentName		VARCHAR(255),
		DataValue			BIGINT,
		[TimeStamp]			DATETIMEOFFSET(1),
		LocalTime			DATETIME,
		DateYear			AS DATEPART(YEAR, LocalTime),
		DateMonth			AS DATEPART(MONTH, LocalTime),
		DateDay				AS DATEPART(DAY, LocalTime),
		DateHour			AS DATEPART(HOUR, LocalTime),
		DateMinute			AS DATEPART(MINUTE, LocalTime)

	);

	--*************************************************************************************************
	--  Declare a temp table to hold the final results with Min and Max values and alarm level
	--*************************************************************************************************
	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#MetricValues'))
	BEGIN
		DROP TABLE #MetricValues;
	END

	CREATE TABLE #MetricValues
	(
		ServerName			VARCHAR(255),
		ApplicationName		VARCHAR(255),
		ComponentName		VARCHAR(255),
		AggregationLevel	VARCHAR(6),
		DataValue			INT,
		MetricTimeStamp		VARCHAR(16),
		MinOverallValue		INT,
		MaxOverallValue		INT,
		AlarmLevel			VARCHAR(8)
	);


	--*************************************************************************************************
	--  Collect the ServerName values for the specified parameters
	--*************************************************************************************************
	INSERT INTO @ServerNames
		SELECT 
			n.NodeID,
			n.Caption
		FROM dbo.NodesData n
			INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
		WHERE ncp.ServerType IN ('APP/SQL', 'CITRIX/SQL', 'SQL')
			AND (ncp.ServerTier = @ServerTier OR @ServerTier = 'ALL')
			AND (n.caption = @ServerName OR @ServerName = 'ALL')

	--*************************************************************************************************
	--  Collect the ApplicationID values for the Application specified as a parameter
	--*************************************************************************************************
	INSERT INTO @AppIDs
		SELECT 
			a.ID, 
			a.[Name],
			sn.ServerName 
		FROM dbo.APM_Application a
			INNER JOIN @ServerNames sn ON a.NodeID = sn.NodeID
		WHERE a.[Name] = @ApplicationName

	--*************************************************************************************************
	--  Collect the ComponentID values for the Component specified as a parameter
	--*************************************************************************************************
	INSERT INTO @CompIDs
		SELECT 
			c.ID, 
			c.[Name],
			a.ID
		FROM dbo.APM_Component c
			INNER JOIN dbo.APM_Application a ON c.ApplicationID = a.ID 
		WHERE c.[Name] = @ComponentName
			AND ApplicationID IN (SELECT AppID FROM @AppIDs);

	--*************************************************************************************************
	--  Individual metric values are keyed by ComponentStatusID values.  These may be stored in the
	--   APM_ComponentStatus_Detail (last 7 days), APM_ComponentStatus_Hourly (last 30 days), or the
	--   APM_ComponentStatus_Daily (last year) tables.  We have to select the ComponentStatusID values
	--   from all 3 tables and UNION them together to get the complete list of keys for the metrics
	--   that we are collecting.  Note that the UNION queries are limited by a WHERE clause that 
	--   restricts them to the date range specified for the report.
	--*************************************************************************************************

	--*************************************************************************************************
	--  Load the temp table with the ComponentStatusID key values
	--*************************************************************************************************
	INSERT INTO #ComponentStatusIDs (ComponentStatusID, ApplicationID, ComponentID, [TimeStamp])
		SELECT 
			ID,
			ApplicationID,
			ComponentID,
			[TimeStamp] AT TIME ZONE 'Central Standard Time'
		FROM
			dbo.APM_ComponentStatus_Detail
		WHERE ApplicationID IN (SELECT AppID FROM @AppIDs)
			AND ComponentID IN (SELECT CompID FROM @CompIDs)
			AND [TimeStamp] BETWEEN @StartDateUTC AND @StopDateUTC
	
		UNION ALL
	
		SELECT 
			ID,
			ApplicationID,
			ComponentID,
			[TimeStamp] AT TIME ZONE 'Central Standard Time'
		FROM
			dbo.APM_ComponentStatus_Hourly
		WHERE ApplicationID IN (SELECT AppID FROM @AppIDs)
			AND ComponentID IN (SELECT CompID FROM @CompIDs)
			AND [TimeStamp] BETWEEN @StartDateUTC AND @StopDateUTC
	
		UNION ALL
	
		SELECT 
			ID,
			ApplicationID,
			ComponentID,
			[TimeStamp] AT TIME ZONE 'Central Standard Time'
		FROM
			dbo.APM_ComponentStatus_Daily
		WHERE ApplicationID IN (SELECT AppID FROM @AppIDs)
			AND ComponentID IN (SELECT CompID FROM @CompIDs)
			AND [TimeStamp] BETWEEN @StartDateUTC AND @StopDateUTC;

	--*************************************************************************************************
	-- SELECT the raw data values into the temp table
	--*************************************************************************************************
	INSERT INTO #RawValues (ServerName, ApplicationName, ComponentName, DataValue, [TimeStamp], LocalTime)
		SELECT 
			ap.NodeName,
			ap.AppName,
			c.CompName,
			pe.AvgStatisticData,
			csd.[TimeStamp],
			NULL
		FROM @AppIds ap
			INNER JOIN @CompIDs c ON c.AppID = ap.AppID
			INNER JOIN #ComponentStatusIDs csd ON csd.ApplicationID = ap.AppID
			INNER JOIN dbo.APM_PortEvidence pe ON pe.ComponentStatusID = csd.ComponentStatusID;

	-- Calculate correct local time from UTC TimeStamp
	UPDATE #RawValues SET LocalTime = DATEADD(MINUTE, DATEPART(tz,[TimeStamp]), [TimeStamp]);


	--*************************************************************************************************
	--  Do a SELECT into the #MetricValues temp table with the appropriate aggregation level.
	--*************************************************************************************************
	IF (@AggregationLevel = 'Daily')
	BEGIN
		INSERT INTO #MetricValues (ServerName, ApplicationName, ComponentName, AggregationLevel, DataValue, MetricTimeStamp)
			SELECT
				 ServerName 
				,ApplicationName
				,ComponentName
				,@AggregationLevel AS AggregationLevel
				,CAST(AVG(DataValue) AS BIGINT) AS DataValue
				,CAST(DateYear AS CHAR(4)) + '/' + RIGHT('00' + CAST(DateMonth AS VARCHAR(2)),2) + '/' + RIGHT('00' + CAST(DateDay AS VARCHAR(2)),2) AS MetricTimeStamp 
			FROM #RawValues
			GROUP BY
				DateYear, 
				DateMonth, 
				DateDay,
				ServerName,
				ApplicationName,
				ComponentName
	END

	IF (@AggregationLevel = 'Hourly')
	BEGIN
		INSERT INTO #MetricValues (ServerName, ApplicationName, ComponentName, AggregationLevel, DataValue, MetricTimeStamp)
			SELECT 
				 ServerName 
				,ApplicationName
				,ComponentName
				,@AggregationLevel AS AggregationLevel
				,CAST(AVG(DataValue) AS BIGINT) AS DataValue
				,CAST(DateYear AS CHAR(4)) + '/' + RIGHT('00' + CAST(DateMonth AS VARCHAR(2)),2) + '/' + RIGHT('00' + CAST(DateDay AS VARCHAR(2)),2) + ' ' + RIGHT('00' + CAST(DateHour AS VARCHAR(2)),2) + ':00'
			FROM #RawValues
			GROUP BY 
				DateYear, 
				DateMonth, 
				DateDay,
				DateHour,
				ServerName,
				ApplicationName,
				ComponentName
	END

	IF (@AggregationLevel = 'Minute')
	BEGIN
		INSERT INTO #MetricValues (ServerName, ApplicationName, ComponentName, AggregationLevel, DataValue, MetricTimeStamp)
			SELECT 
				 ServerName 
				,ApplicationName
				,ComponentName
				,@AggregationLevel AS AggregationLevel
				,CAST(AVG(DataValue) AS BIGINT) AS DataValue
				,CAST(DateYear AS CHAR(4)) + '/' + RIGHT('00' + CAST(DateMonth AS VARCHAR(2)),2) + '/' + RIGHT('00' + CAST(DateDay AS VARCHAR(2)),2) + ' ' + RIGHT('00' + CAST(DateHour AS VARCHAR(2)),2) + ':' + RIGHT('00' + CAST(DateMinute AS VARCHAR(2)),2) 
			FROM #RawValues
			GROUP BY 
				DateYear, 
				DateMonth, 
				DateDay,
				DateHour,
				DateMinute,
				ServerName,
				ApplicationName,
				ComponentName
	END


	--*************************************************************************************************
	-- Calculate minimum and maximum metric values and alarm level
	--*************************************************************************************************

	DECLARE @X				INT = 1;
	DECLARE @RowCount		INT = (SELECT MAX(ServerNamesID) FROM @ServerNames);
	DECLARE @ServerName1	VARCHAR(255) = '';
	DECLARE @Max			INT = 0;
	DECLARE @Min			INT = 0;

	WHILE (@X <= @RowCount)
	BEGIN
		SET @ServerName1 = (SELECT ServerName FROM @ServerNames WHERE ServerNamesID = @X);
		SET @Max = (SELECT MAX(DataValue) FROM #MetricValues WHERE ServerName = @ServerName1 GROUP BY ServerName)
		SET @Min = (SELECT MIN(DataValue) FROM #MetricValues WHERE ServerName = @ServerName1 GROUP BY ServerName)

		UPDATE #MetricValues SET
			MinOverallValue = @Min,
			MaxOverallValue = @Max,
			AlarmLevel = 'Normal'
		WHERE ServerName = @ServerName1;

		IF (@Comparison = '<=')
		BEGIN
			IF (@Min <= @CriticalLevel) UPDATE #MetricValues SET AlarmLevel = 'Critical' WHERE ServerName = @ServerName1;
			ELSE IF (@Min <= @WarningLevel AND @WarningLevel <> -1) UPDATE #MetricValues SET AlarmLevel = 'Warning' WHERE ServerName = @ServerName1;
		END
		ELSE
		IF (@Comparison = '>=')
		BEGIN
			IF (@Max >= @CriticalLevel) UPDATE #MetricValues SET AlarmLevel = 'Critical' WHERE ServerName = @ServerName1;
			ELSE IF (@Max >= @WarningLevel AND @WarningLevel <> -1) UPDATE #MetricValues SET AlarmLevel = 'Warning' WHERE ServerName = @ServerName1;
		END

		SET @X = @X + 1;
	END

	--*************************************************************************************************
	--  Do a final SELECT of the resultset from #MetricValues
	--*************************************************************************************************
	SELECT
		ServerName,
		ApplicationName,
		ComponentName,
		AggregationLevel,
		DataValue,
		MetricTimeStamp,
		MinOverallValue,
		MaxOverallValue,
		AlarmLevel
	FROM #MetricValues
	WHERE (AlarmLevel <> 'Normal' AND @IncludeAlarmOnly = 1) OR @IncludeAlarmOnly = 0
	ORDER BY ServerName, MetricTimeStamp;		


	--*************************************************************************************************
	--  Drop the temp tables to clean up
	--*************************************************************************************************
	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#ComponentStatusIDs'))
	BEGIN
		DROP TABLE #ComponentStatusIDs;
	END
	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#RawValues'))
	BEGIN
		DROP TABLE #RawValues;
	END
	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#MetricValues'))
	BEGIN
		DROP TABLE #MetricValues;
	END

END
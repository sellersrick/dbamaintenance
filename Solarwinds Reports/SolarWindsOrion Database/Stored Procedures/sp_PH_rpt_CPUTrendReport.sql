USE [SolarWindsOrion]
GO
/****** Object:  StoredProcedure [dbo].[sp_PH_rpt_CPUTrendReport]    Script Date: 3/6/2018 9:28:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   PROCEDURE [dbo].[sp_PH_rpt_CPUTrendReport]
	@ServerTier				VARCHAR(3) = 'ALL',
	@ServerName				VARCHAR(256) = NULL,
	@DEBUG					BIT = 0

/*=========================================================
NAME:			dbo.sp_PH_rpt_CPUTrendReport
DESCRIPTION:	Retrieves the for the disk trend report from the 
				SolarWindsOrion repository database for the
				specified parameters.
INPUTS:			@ServerTier - the tier of the server(s) for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					server(s) to which this application is attached.
				@ServerName - the name of the server for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					server to which this application is attached.
				@DEBUG - bit flag to print intermediate data to help in debugging
OUTPUTS:		None
RETURN:			None
RESULTS:		Resultset of the values for the specified parameters
ERRORS:			50000 - invalid server name
DEPENDENCIES:	dbo.CPULoad (Solarwinds view) 
				dbo.NodesCustomProperties
				dbo.NodesData
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20180228	Initial Author
  rick.sellers@premisehealth.com	20180417	Changed to capture ALL database servers
									
=========================================================
--DEBUG--

DECLARE @ServerTier				VARCHAR(3) = 'ALL';
DECLARE @ServerName				VARCHAR(256) = 'ALL';
DECLARE @DEBUG					BIT = 0;

EXEC dbo.sp_PH_rpt_CPUTrendReport @ServerTier, @ServerName, @DEBUG
    
=========================================================*/
AS
BEGIN

	--*************************************************************************************************
	--  Verify ServerName and that it is a SQL Server
	--*************************************************************************************************
	IF (@ServerName IS NULL) SET @ServerName = 'ALL';

	IF NOT EXISTS (SELECT 1 
					FROM dbo.NodesData n
						INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
					WHERE n.Caption = @ServerName OR @ServerName = 'ALL'
						AND ncp.ServerType LIKE '%SQL%'
				  )
	BEGIN
		;THROW 50000, 'Specified @ServerName was not found.', 1;
	END

	--*************************************************************************************************
	-- declare table variable to hold server information
	--*************************************************************************************************

	DECLARE @ServerNames TABLE
	(
		ServerNamesID		INT IDENTITY(1,1),
		NodeID				INT,
		ServerName			VARCHAR(255)
	);

	--*************************************************************************************************
	--  Collect the ServerName values for the specified parameters
	--*************************************************************************************************
	INSERT INTO @ServerNames
		SELECT 
			n.NodeID,
			n.Caption
		FROM dbo.NodesData n
			INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
		WHERE ncp.ServerType LIKE '%SQL%'
			AND (ncp.ServerTier = @ServerTier OR @ServerTier = 'ALL')
			AND (n.caption = @ServerName OR @ServerName = 'ALL');

	--*************************************************************************************************
	--  drop and create temp table
	--*************************************************************************************************
	BEGIN TRY DROP TABLE #TempCPULoad END TRY BEGIN CATCH END CATCH;

	CREATE TABLE #TempCPULoad
	(
		TempCPULoadID			INT IDENTITY(1,1),
		NodeID					INT,
		ServerName				VARCHAR(256),
		WeekNumber				TINYINT,
		MinLoad					TINYINT,
		MaxLoad					TINYINT,
		AvgLoad					TINYINT,
		SortOrderWeekNumber		SMALLINT,
		DisplayWeekNumber		CHAR(7)
	)

	--*************************************************************************************************
	--  initial load of temp table from Solarwinds view
	--*************************************************************************************************
	INSERT INTO #TempCPULoad (NodeID, ServerName, WeekNumber, MinLoad, MaxLoad, AvgLoad)
		SELECT 
			c.NodeID,
			ServerName,
			DATEPART(WEEK,[DateTime]),
			MIN(MinLoad),
			MAX(MaxLoad),
			AVG(AvgLoad)
		FROM dbo.CPULoad c
			INNER JOIN @ServerNames s ON s.NodeID = c.NodeID
		GROUP BY c.NodeID, ServerName, DATEPART(WEEK,[DateTime])
		ORDER BY NodeID

	--*************************************************************************************************
	--  Calculate max week number and current week number for sort order week number
	--*************************************************************************************************
	DECLARE @MaxWeekNumber TINYINT = (SELECT MAX(WeekNumber) FROM #TempCPULoad);
	DECLARE @CurrentWeekNumber TINYINT = DATEPART(WEEK, GETDATE());

	IF (@DEBUG = 1)
	BEGIN
		PRINT '@MaxWeekNumber = ' + CAST(@MaxWeekNumber AS VARCHAR(2));
		PRINT '@CurrentWeekNumber = ' + CAST(@CurrentWeekNumber AS VARCHAR(2));
	END;

	IF (@DEBUG = 1)
	BEGIN
		SELECT * FROM #TempCPULoad;
	END;

	--*************************************************************************************************
	--  force sort order week number to be the same as the week number
	--*************************************************************************************************
	UPDATE #TempCPULoad SET
		SortOrderWeekNumber = WeekNumber;

	--*************************************************************************************************
	--  override the sort order week number value to get the records into the correct week order if
	--   we crossed a year boundary
	--*************************************************************************************************
	UPDATE #TempCPULoad SET
		SortOrderWeekNumber = CAST(WeekNumber AS SMALLINT) - CAST(@MaxWeekNumber AS SMALLINT)
	WHERE WeekNumber > @CurrentWeekNumber;

	UPDATE #TempCPULoad SET
		DisplayWeekNumber = CAST(YEAR(GETDATE()) AS CHAR(4)) + '-' + RIGHT('00' + CAST(SortOrderWeekNumber AS VARCHAR(2)), 2)
	WHERE SortOrderWeekNumber > 0;

	UPDATE #TempCPULoad SET
		DisplayWeekNumber = CAST(YEAR(DATEADD(YEAR, -1, GETDATE())) AS CHAR(4)) + '-' + RIGHT('00' + CAST(@MaxWeekNumber - ABS(SortOrderWeekNumber) AS VARCHAR(2)), 2)
	WHERE SortOrderWeekNumber <= 0;

	--*************************************************************************************************
	--  Select final results
	--*************************************************************************************************
	SELECT 
		NodeID,
		ServerName,
		MinLoad,
		MaxLoad,
		AvgLoad,
		SortOrderWeekNumber,
		DisplayWeekNumber
 
	FROM #TempCPULoad 
	ORDER BY NodeID, SortOrderWeekNumber;

END;
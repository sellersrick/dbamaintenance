USE [SolarWindsOrion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[sp_PH_rpt_GetServerNames]
	@ServerTier	CHAR(3)
/*=========================================================
NAME:			dbo.sp_PH_rpt_GetServerNames
DESCRIPTION:	Retrieves the names of the servers for which
					applications and components have been attached.
INPUTS:			None
OUTPUTS:		None
RETURN:			None
RESULTS:		Resultset of the ServerNames
ERRORS:			None
DEPENDENCIES:	dbo.APM_Application
				dbo.APM_Component
				dbo.NodesCustomProperties
				dbo.NodesData
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20171229	Initial Author
									
=========================================================
--DEBUG--

DECLARE @ServerTier CHAR(3) = 'ALL';

EXEC dbo.sp_PH_rpt_GetServerNames @ServerTier
    
=========================================================*/
AS
BEGIN
	SELECT 'ALL' AS ServerName
	UNION 
	SELECT
		n.Caption AS ServerName
	FROM dbo.NodesData n
		INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
		INNER JOIN dbo.APM_Application a ON a.NodeID = n.NodeID
		INNER JOIN dbo.APM_Component c ON c.ApplicationID = a.ID
	WHERE ncp.ServerType IN ('APP/SQL', 'Citrix/SQL', 'SQL')
		AND a.[Name] LIKE '_DBA%'
		AND (ServerTier = @ServerTier OR @ServerTier = 'ALL');
END
USE [SolarWindsOrion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[sp_PH_rpt_GetCPUValues]
	@ServerTier			VARCHAR(3) = 'ALL',
	@ServerName			VARCHAR(256) = NULL,
	@AggregationLevel	VARCHAR(10) = 'Minute',
	@StartDate			DATETIME = NULL,
	@StopDate			DATETIME = NULL,
	@WarningLevel		INT = 80,
	@CriticalLevel		INT = 90,
	@IncludeAlarmOnly	BIT = 0

/*=========================================================
NAME:			dbo.sp_PH_rpt_GetCPUValues
DESCRIPTION:	Retrieves the collected metric values from the 
				SolarWindsOrion repository database for the
				specified parameters.
INPUTS:			@ServerTier - the tier of the server(s) for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					server(s) to which this application is attached.
				@ServerName - the name of the server for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					server to which this application is attached.
				@AggregationLevel - the level to which aggregation
					done in the returned dataset.  valid values are:
						Minute - the lowest level at which metric
							collection occurs.  This is in 
							10 Minute intervals,
						Hourly - Aggregated for each hour.
						Daily - Aggregated for each day
					Lowest-level aggregates are available for 7 days,
						then aggregated to hourly level for 30 days, 
						then aggregated to daily levels for one year.
						These are the defaults for Orion and have been
						left at these settings.  If the aggregation 
						level specified is not valid for the date range
						specified, it is promoted automatically to the
						lowest aggregation level that is valid.
				@StartDate - the starting date and time for the results
				@StopTime - the ending date and time for the results
				@WarningLevel - the CPU percentage that triggers a Warning level
				@CriticalLevel - the CPU percentage that triggers a Critical level
				@IncludeAlarmOnly - bit flag to only return servers in an alarm state if set to 1
OUTPUTS:		None
RETURN:			None
RESULTS:		Resultset of the metric values for the specified parameters
ERRORS:			50000 - invalid server name
				50001 - invalid aggregation level specified
				50002 - @StartDate is not a valid date
				50003 - @StopDate is not a valid date
				50004 - invalid date range specified
				50005 - @WarningLevel must be between 1 and 100
				50006 - @CriticalLevel must be between 1 and 100
				50007 - @WarningLevel must be less than @CriticalLevel
				50008 - More than 60 data points selected
				50009 - @StopDate cannot be greater than today
DEPENDENCIES:	dbo.APM_CPULoad (Solarwinds view)
				dbo.NodesCustomProperties
				dbo.NodesData
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20180109	Initial Author
  rick.sellers@premisehealth.com	20180417	Changed to capture ALL database servers
									
=========================================================
--DEBUG--

DECLARE @ServerTier				VARCHAR(3) = 'ALL';
DECLARE @ServerName				VARCHAR(256) = 'dbwarehouse';
DECLARE @AggregationLevel		VARCHAR(10) =  'Daily';
DECLARE @StartDate				DATETIME = '2017-12-11';
DECLARE @StopDate				DATETIME = '2018-01-11';
DECLARE @WarningLevel			INT = 70;
DECLARE @CriticalLevel			INT = 90;
DECLARE @IncludeAlarmOnly		BIT = 0;

EXEC dbo.sp_PH_rpt_GetCPUValues @ServerTier, @ServerName, @AggregationLevel, @StartDate, @StopDate, @WarningLevel, @CriticalLevel, @IncludeAlarmOnly
    
=========================================================*/
AS
BEGIN
	--*************************************************************************************************
	--  Verify ServerName and that it is a SQL Server
	--*************************************************************************************************
	IF (@ServerName IS NULL) SET @ServerName = 'ALL';

	IF NOT EXISTS (SELECT 1 
					FROM dbo.NodesData n
						INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
					WHERE n.Caption = @ServerName OR @ServerName = 'ALL'
						AND ncp.ServerType LIKE '%SQL%'
				  )
	BEGIN
		;THROW 50000, 'Specified @ServerName was not found.', 1;
	END

	--*************************************************************************************************
	--  Verify date range
	--*************************************************************************************************
	IF (ISDATE(@StartDate)) <> 1
	BEGIN
		;THROW 50002, '@StartDate value is not a valid date.', 1;
	END

	IF (ISDATE(@StopDate)) <> 1
	BEGIN
		;THROW 50003, '@StopDate value is not a valid date.', 1;
	END

	IF (DATEDIFF(DAY, GETDATE(), @StopDate) > 1)
	BEGIN
		;THROW 50009, '@StopDate cannot be greater than today.', 1;
	END

	IF (@StopDate <= @StartDate)
	BEGIN
		;THROW 50004, '@StopDate must be >= @StartDate.', 1;
	END

	--*************************************************************************************************
	--  Verify Aggregation Level
	--*************************************************************************************************
	IF (@AggregationLevel NOT IN ('Daily', 'Hourly', 'Minute'))
	BEGIN
		;THROW 50001, 'AggregationLevel must be ''Daily'', ''Hourly'', or ''Minute''.', 1;
	END

	-- If more than twelve hours time difference or StopDate more than 7 days ago, change the
	--  aggregation level to hourly, but DO NOT override Daily aggregation level
	IF ((DATEDIFF(MINUTE, @StartDate, @StopDate) > 720 OR DATEDIFF(DAY, @StopDate, GETDATE()) > 7) AND @AggregationLevel <> 'Daily')
	BEGIN
		SET @AggregationLevel = 'Hourly';
	END

	-- If more than 13 days time difference or stop date > 30 days ago, change the
	--  aggregation level to daily
	IF (DATEDIFF(DAY, @StartDate, @StopDate) >= 14 OR DATEDIFF(DAY, @StopDate, GETDATE()) > 30)
	BEGIN
		SET @AggregationLevel = 'Daily';
	END

	-- If more than 60 data points are selected, make the user narrow the date range.
	IF (@AggregationLevel = 'Daily')
	BEGIN
		IF (DATEDIFF(DAY, @StartDate, @StopDate) > 60)
		BEGIN
			;THROW 50008, 'More than 60 data points selected (Daily).  Please narrow the date range.', 1;
		END
	END

	IF (@AggregationLevel = 'Hourly')
	BEGIN
		IF (DATEDIFF(HOUR, @StartDate, @StopDate) > 60)
		BEGIN
			;THROW 50008, 'More than 60 data points selected (Hourly).  Please narrow the date range.', 1;
		END
	END

	--*************************************************************************************************
	--  Verify Warning and Critical levels
	--*************************************************************************************************
	IF (NOT @WarningLevel BETWEEN -1 and 100)
	BEGIN
		;THROW 50005, '@WarningLevel must be between -1 and 100.', 1;
	END

	IF (NOT @CriticalLevel BETWEEN 1 and 100)
	BEGIN
		;THROW 50006, '@CriticalLevel must be between 1 and 100.', 1;
	END

	IF (@WarningLevel >= @CriticalLevel)
	BEGIN
		;THROW 50007, '@WarningLevel must be less than @CriticalLevel.', 1;
	END

	--*************************************************************************************************
	-- Declare a table variable of the appropriate servers based on the parameters
	--*************************************************************************************************
	DECLARE @ServerNames TABLE
	(
		ServerNamesID	INT IDENTITY(1,1),
		NodeID			INT,
		ServerName		VARCHAR(255)
	)

	INSERT INTO @ServerNames
		SELECT 
			n.NodeID,
			n.Caption
		FROM dbo.NodesData n
			INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
		WHERE ncp.ServerType LIKE '%SQL%'
			AND (ncp.ServerTier = @ServerTier OR @ServerTier = 'ALL')
			AND (n.caption = @ServerName OR @ServerName = 'ALL')

	--*************************************************************************************************
	--  Declare a temp table to hold the CPULoad aggregated values.  Note that we have five
	--   computed columns that automatically split out the date parts so that we do not have to run 
	--   functions in the SELECT or GROUP BY clauses
	--*************************************************************************************************
	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#CPULoads'))
	BEGIN
		DROP TABLE #CPULoads;
	END

	CREATE TABLE #CPULoads
	(
		ServerName			VARCHAR(255),
		MinLoad				TINYINT,
		MaxLoad				TINYINT,
		AvgLoad				TINYINT,
		LocalTime			DATETIME,
		DateYear			AS DATEPART(YEAR, LocalTime),
		DateMonth			AS DATEPART(MONTH, LocalTime),
		DateDay				AS DATEPART(DAY, LocalTime),
		DateHour			AS DATEPART(HOUR, LocalTime),
		DateMinute			AS DATEPART(MINUTE, LocalTime)
	);

	--*************************************************************************************************
	--  Declare a temp table to hold the final results
	--*************************************************************************************************
	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#CPUResults'))
	BEGIN
		DROP TABLE #CPUResults;
	END

	CREATE TABLE #CPUResults
	(
		ServerName			VARCHAR(255),
		AggregationLevel	VARCHAR(9),
		MetricTimeStamp		VARCHAR(16),
		MinLoad				TINYINT,
		MaxLoad				TINYINT,
		AvgLoad				TINYINT,
		MaxAvgLoad			TINYINT,
		UtilizationLevel	VARCHAR(8)
	);

	--*************************************************************************************************
	--  Individual CPULoad values are retrieved from the dbo.CPULoad Solarwinds view.  These are stored
	--   in the CPULoad_Detail view (last 7 days), CPULoad_Hourly view (last 30 days), or the
	--   CPULoad_Daily view (last year) tables.  We use the Solarwinds view dbo.CPULoad to retrieve the
	--   values from all 3 views (it automatically UNIONs them together) to get the complete list of 
	--   values for the date range specified for the report.
	--*************************************************************************************************

	--*************************************************************************************************
	--  Load the temp table with the CPULoad values
	--*************************************************************************************************
	INSERT INTO #CPULoads (ServerName, MinLoad, MaxLoad, AvgLoad, LocalTime)
		SELECT 
			ServerName,
			MinLoad,
			MaxLoad,
			AvgLoad,
			[DateTime]
		FROM dbo.CPULoad cl
  			INNER JOIN @ServerNames n ON n.nodeid = cl.NodeID
		WHERE cl.[DateTime] BETWEEN @StartDate AND @StopDate

	----*************************************************************************************************
	---- Create index on the temp table
	----*************************************************************************************************
	--CREATE NONCLUSTERED INDEX PK_CPULoads ON #CPULoads (ServerName, LocalTime);
	--CREATE NONCLUSTERED INDEX IX_CPULoads ON #CPULoads (ServerName, MaxLoad);

	--*************************************************************************************************
	-- Aggregate the values, respecting the @AggregationLevel.  
	--*************************************************************************************************
	IF (@AggregationLevel = 'Daily')
	BEGIN
		INSERT INTO #CPUResults (ServerName, AggregationLevel, MetricTimeStamp, MinLoad, MaxLoad, AvgLoad)
			SELECT
				 ServerName 
				,@AggregationLevel AS AggregationLevel
				,CAST(DateYear AS CHAR(4)) + '/' + RIGHT('00' + CAST(DateMonth AS VARCHAR(2)),2) + '/' + RIGHT('00' + CAST(DateDay AS VARCHAR(2)),2) AS MetricTimeStamp 
				,MIN(MinLoad) AS MinCPULoad
				,MAX(MaxLoad) AS MaxCPULoad
				,AVG(AvgLoad) AS AvgCPULoad
			FROM #CPULoads cl
			GROUP BY 
				ServerName,
				cl.DateYear, 
				cl.DateMonth, 
				cl.DateDay
			ORDER BY ServerName, cl.DateYear, cl.DateMonth, cl.DateDay
	END

	IF (@AggregationLevel = 'Hourly')
	BEGIN
		INSERT INTO #CPUResults (ServerName, AggregationLevel, MetricTimeStamp, MinLoad, MaxLoad, AvgLoad)
			SELECT 
				 ServerName 
				,@AggregationLevel AS AggregationLevel
				,CAST(DateYear AS CHAR(4)) + '/' + RIGHT('00' + CAST(DateMonth AS VARCHAR(2)),2) + '/' + RIGHT('00' + CAST(DateDay AS VARCHAR(2)),2) + ' ' + RIGHT('00' + CAST(cl.DateHour AS VARCHAR(2)), 2) + ':00' AS MetricTimeStamp 
				,MIN(MinLoad) AS MinCPULoad
				,MAX(MaxLoad) AS MaxCPULoad
				,AVG(AvgLoad) AS AvgCPULoad
			FROM #CPULoads cl
			GROUP BY 
				ServerName,
				cl.DateYear, 
				cl.DateMonth, 
				cl.DateDay,
				cl.DateHour
			ORDER BY ServerName, cl.DateYear, cl.DateMonth, cl.DateDay, cl.DateHour
	END

	IF (@AggregationLevel = 'Minute')
	BEGIN
		INSERT INTO #CPUResults (ServerName, AggregationLevel, MetricTimeStamp, MinLoad, MaxLoad, AvgLoad)
			SELECT 
				 ServerName 
				,@AggregationLevel AS AggregationLevel
				,CAST(DateYear AS CHAR(4)) + '/' + RIGHT('00' + CAST(DateMonth AS VARCHAR(2)),2) + '/' + RIGHT('00' + CAST(DateDay AS VARCHAR(2)),2) + ' ' + RIGHT('00' + CAST(cl.DateHour AS VARCHAR(2)), 2) + ':'  + RIGHT('00' + CAST(cl.DateMinute AS VARCHAR(2)), 2)AS MetricTimeStamp 
				,MIN(MinLoad) AS MinCPULoad
				,MAX(MaxLoad) AS MaxCPULoad
				,AVG(AvgLoad) AS AvgCPULoad
			FROM #CPULoads cl
			GROUP BY 
				ServerName,
				cl.DateYear, 
				cl.DateMonth, 
				cl.DateDay,
				cl.DateHour,
				cl.DateMinute
			ORDER BY ServerName, cl.DateYear, cl.DateMonth, cl.DateDay, cl.DateHour, cl.DateMinute
	END

	--*************************************************************************************************
	-- Calculate maximum CPU load and utilization level
	--*************************************************************************************************

	DECLARE @X				INT = 1;
	DECLARE @RowCount		INT = (SELECT MAX(ServerNamesID) FROM @ServerNames);
	DECLARE @ServerName1	VARCHAR(255) = '';
	DECLARE @Max			TINYINT = 0;

	WHILE (@X <= @RowCount)
	BEGIN
		SET @ServerName1 = (SELECT ServerName FROM @ServerNames WHERE ServerNamesID = @X);
		SET @Max = (SELECT MAX(AvgLoad) FROM #CPUResults WHERE ServerName = @ServerName1 GROUP BY ServerName)

		UPDATE #CPUResults SET
			MaxAvgLoad = @Max,
			UtilizationLevel = 
				CASE
					WHEN @Max >= @CriticalLevel THEN 'Critical'
					WHEN @Max >= @WarningLevel AND @WarningLevel <> -1 THEN 'Warning'
					ELSE 'Normal'
				END
		WHERE ServerName = @ServerName1;

		SET @X = @X + 1;
	END

	--*************************************************************************************************
	--  Do the final SELECT to get the CPU load values
	--*************************************************************************************************
	SELECT
		ServerName,
		@AggregationLevel AS AggregationLevel,
		MetricTimeStamp,
		MinLoad,
		MaxLoad,
		AvgLoad,
		MaxAvgLoad,
		UtilizationLevel
	FROM #CPUResults
	WHERE (UtilizationLevel <> 'Normal' AND @IncludeAlarmOnly = 1) OR @IncludeAlarmOnly = 0

	--*************************************************************************************************
	--  Drop the temp table to clean up
	--*************************************************************************************************
	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#CPULoads'))
	BEGIN
		DROP TABLE #CPULoads;
	END

	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#CPUResults'))
	BEGIN
		DROP TABLE #CPUResults;
	END

END
USE [SolarWindsOrion]
GO
/****** Object:  StoredProcedure [dbo].[sp_PH_rpt_CPUTrendReport_48Hours]    Script Date: 4/30/2018 2:47:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_PH_rpt_CPUTrendReport_48Hours]
	@ServerTier				VARCHAR(3) = 'ALL',
	@ServerName				VARCHAR(256) = NULL,
	@DEBUG					BIT = 0

/*=========================================================
NAME:			dbo.sp_PH_rpt_CPUTrendReport_48Hours
DESCRIPTION:	Retrieves the for the disk trend report from the 
				SolarWindsOrion repository database for the
				Last 48 jours aggregated by hour for the 
				specified parameters.
INPUTS:			@ServerTier - the tier of the server(s) for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					server(s) to which this application is attached.
				@ServerName - the name of the server for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					server to which this application is attached.
				@DEBUG - bit flag to print intermediate data to help in debugging
OUTPUTS:		None
RETURN:			None
RESULTS:		Resultset of the values for the specified parameters
ERRORS:			50000 - invalid server name
DEPENDENCIES:	dbo.CPULoad (Solarwinds view) 
				dbo.NodesCustomProperties
				dbo.NodesData
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20180228	Initial Author
  rick.sellers@premisehealth.com	20180417	Changed to capture ALL database servers
									
=========================================================
--DEBUG--

DECLARE @ServerTier				VARCHAR(3) = 'ALL';
DECLARE @ServerName				VARCHAR(256) = 'ALL';
DECLARE @DEBUG					BIT = 1;

EXEC dbo.sp_PH_rpt_CPUTrendReport_48Hours @ServerTier, @ServerName, @DEBUG
    
=========================================================*/
AS
BEGIN

	--*************************************************************************************************
	--  Verify ServerName and that it is a SQL Server
	--*************************************************************************************************
	IF (@ServerName IS NULL) SET @ServerName = 'ALL';

	IF NOT EXISTS (SELECT 1 
					FROM dbo.NodesData n
						INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
					WHERE n.Caption = @ServerName OR @ServerName = 'ALL'
						AND ncp.ServerType LIKE '%SQL%'
				  )
	BEGIN
		;THROW 50000, 'Specified @ServerName was not found.', 1;
	END

	--*************************************************************************************************
	-- declare table variable to hold server information
	--*************************************************************************************************

	DECLARE @ServerNames TABLE
	(
		ServerNamesID		INT IDENTITY(1,1),
		NodeID				INT,
		ServerName			VARCHAR(255)
	);

	--*************************************************************************************************
	--  Collect the ServerName values for the specified parameters
	--*************************************************************************************************
	INSERT INTO @ServerNames
		SELECT 
			n.NodeID,
			n.Caption
		FROM dbo.NodesData n
			INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
		WHERE ncp.ServerType LIKE '%SQL%'
			AND (ncp.ServerTier = @ServerTier OR @ServerTier = 'ALL')
			AND (n.caption = @ServerName OR @ServerName = 'ALL');

	--*************************************************************************************************
	--  drop and create temp table
	--*************************************************************************************************
	BEGIN TRY DROP TABLE #TempCPULoad END TRY BEGIN CATCH END CATCH;

	CREATE TABLE #TempCPULoad
	(
		TempCPULoadID			INT IDENTITY(1,1),
		NodeID					INT,
		ServerName				VARCHAR(256),
		[DateTime]				DATETIME,
		DateYear				AS DATEPART(YEAR, [DateTime]),
		DateMonth				AS DATEPART(MONTH, [DateTime]),
		DateDay					AS DATEPART(DAY, [DateTime]),
		DateHour				AS DATEPART(HOUR, [DateTime]),
		MinLoad					TINYINT,
		MaxLoad					TINYINT,
		AvgLoad					TINYINT
	)

	--*************************************************************************************************
	--  drop and create temp table
	--*************************************************************************************************
	BEGIN TRY DROP TABLE #TempCPUResults END TRY BEGIN CATCH END CATCH;

	CREATE TABLE #TempCPUResults
	(
		TempCPUResultsID		INT IDENTITY(1,1),
		ServerName				VARCHAR(256),
		HourTimeStamp			VARCHAR(16),
		MinLoad					TINYINT,
		MaxLoad					TINYINT,
		AvgLoad					TINYINT
	)

	--*************************************************************************************************
	--  initial load of temp table from Solarwinds view
	--*************************************************************************************************
	INSERT INTO #TempCPULoad (NodeID, ServerName, [DateTime], MinLoad, MaxLoad, AvgLoad)
		SELECT 
			c.NodeID,
			ServerName,
			[DateTime],
			MinLoad,
			MaxLoad,
			AvgLoad
		FROM dbo.CPULoad c
			INNER JOIN @ServerNames s ON s.NodeID = c.NodeID
		WHERE [DateTime] BETWEEN DATEADD(DAY, -2, GETDATE()) AND GETDATE()
		ORDER BY NodeID, [DateTime]

	IF (@DEBUG = 1)
	BEGIN
		SELECT * FROM #TempCPULoad
		ORDER BY ServerName, [DateTime];
	END

	INSERT INTO #TempCPUResults (ServerName, HourTimeStamp, MaxLoad, MinLoad, AvgLoad)
		SELECT
			ServerName,
			CAST(DateYear AS CHAR(4)) + '/' + RIGHT('00' + CAST(DateMonth AS VARCHAR(2)),2) + '/' + RIGHT('00' + CAST(DateDay AS VARCHAR(2)),2) + ' ' + RIGHT('00' + CAST(DateHour AS VARCHAR(2)), 2) + ':00' AS HourTimeStamp,
			MAX(MaxLoad),
			MIN(MinLoad),
			AVG(AvgLoad)
		FROM #TempCPULoad
		GROUP BY ServerName, DateYear, DateMonth, DateDay, DateHour

	--*************************************************************************************************
	--  Select final results
	--*************************************************************************************************
	SELECT 
		ServerName,
		HourTimeStamp,
		MinLoad,
		MaxLoad,
		AvgLoad
	FROM #TempCPUResults
	GROUP BY ServerName, HourTimeStamp, MinLoad, MaxLoad, AvgLoad
	ORDER BY ServerName, HourTimeStamp;

END;
USE [SolarWindsOrion]
GO
/****** Object:  StoredProcedure [dbo].[sp_PH_rpt_GetServerNames_Infrastructure]    Script Date: 4/18/2018 10:23:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create  PROCEDURE [dbo].[sp_PH_rpt_GetServerNames_Infrastructure]
/*=========================================================
NAME:			dbo.sp_PH_rpt_GetServerNames_Infrastructure
DESCRIPTION:	Retrieves the names of the servers for which
					applications and components have been attached.
INPUTS:			None
OUTPUTS:		None
RETURN:			None
RESULTS:		Resultset of the ServerNames
ERRORS:			None
DEPENDENCIES:	dbo.NodesCustomProperties
				dbo.NodesData
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20180418	Initial Author
									
=========================================================
--DEBUG--

EXEC dbo.sp_PH_rpt_GetServerNames_Infrastructure
    
=========================================================*/
AS
BEGIN
	SELECT 'ALL' AS ServerName
	UNION 
	SELECT
		n.Caption AS ServerName
	FROM dbo.NodesData n
		INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
	WHERE ncp.ServerType LIKE '%FileServer%'
END
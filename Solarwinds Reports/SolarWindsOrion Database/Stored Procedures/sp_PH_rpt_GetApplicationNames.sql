USE [SolarWindsOrion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[sp_PH_rpt_GetApplicationNames]
	@ServerTier		VARCHAR(3) = 'ALL',
	@ServerName		VARCHAR(256) = 'ALL'

/*=========================================================
NAME:			dbo.sp_PH_rpt_GetApplicationNames
DESCRIPTION:	Retrieves the names of the Applications which
					have been defined for SQL-type servers
					and attached to at least one server.  If
					a server name is specified, the list is 
					restricted to the applications attached
					to the specified server.
INPUTS:			@ServerTier - the tier of the server(s) for which 
					to retrieve the application names.  A value of 
					'ALL' for this parameter retrieves application
					names for all server(s) with an attached application.
				@ServerName - name of the server for which
					the application names are to be retrieved.
					If the @ServerName parameter is 'ALL', then
					the list is only resticted by the server tier
					parameter.
OUTPUTS:		None
RETURN:			None
RESULTS:		Resultset of the ApplicationNames
ERRORS:			None
DEPENDENCIES:	dbo.APM_Application
				dbo.NodesCustomProperties
				dbo.NodesData
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20171229	Initial Author
  rick.sellers@premisehealth.com	20180417	Changed to capture ALL database servers
									
=========================================================
--DEBUG--
DECLARE @ServerTier VARCHAR(3) = 'ALL';
DECLARE @ServerName	VARCHAR(256) = 'ALL';

EXEC dbo.sp_PH_rpt_GetApplicationNames @ServerTier, @ServerName;
    
=========================================================*/
AS
BEGIN
	SELECT DISTINCT
		a.[Name] AS ApplicationName
	FROM dbo.APM_Application a
		INNER JOIN dbo.NodesData n ON n.NodeID = a.NodeID
		INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
	WHERE ncp.ServerType LIKE '%SQL%'
		AND (ncp.ServerTier = @ServerTier OR @ServerTier = 'ALL')
		AND (n.Caption = @ServerName OR @ServerName = 'ALL')
		AND a.[Name] LIKE '_DBA_Metrics%'
	ORDER BY a.[Name];
END
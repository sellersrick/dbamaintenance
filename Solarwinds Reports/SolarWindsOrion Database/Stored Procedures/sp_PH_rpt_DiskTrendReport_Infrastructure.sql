USE [SolarWindsOrion]
GO
/****** Object:  StoredProcedure [dbo].[sp_PH_rpt_DiskTrendReport_Infrastructure]    Script Date: 4/18/2018 2:36:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   PROCEDURE [dbo].[sp_PH_rpt_DiskTrendReport_Infrastructure]
	@ServerName				VARCHAR(256) = NULL,
	@DEBUG					BIT = 0

/*=========================================================
NAME:			dbo.sp_PH_rpt_DiskTrendReport_Infrastructure
DESCRIPTION:	Retrieves the for the disk trend report from the 
				SolarWindsOrion repository database for the
				specified parameters.
INPUTS:			@ServerName - the name of the server for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					servers.
				@DEBUG - bit flag to print intermediate data to help in debugging
OUTPUTS:		None
RETURN:			None
RESULTS:		Resultset of the values for the specified parameters
ERRORS:			50000 - invalid server name
DEPENDENCIES:	dbo.Volumes 
				dbo.VolumeUsage (Solarwinds view)
				dbo.NodesCustomProperties
				dbo.NodesData
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20180227	Initial Author
									
=========================================================
--DEBUG--

DECLARE @ServerName				VARCHAR(256) = 'ALL';
DECLARE @DEBUG					BIT = 0;

EXEC dbo.sp_PH_rpt_DiskTrendReport_Infrastructure @ServerName, @DEBUG
    
=========================================================*/
AS
BEGIN

	--*************************************************************************************************
	--  Verify ServerName and that it is an Infrastructure Server
	--*************************************************************************************************
	IF (@ServerName IS NULL) SET @ServerName = 'ALL';

	IF NOT EXISTS (SELECT 1 
					FROM dbo.NodesData n
						INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
					WHERE (n.Caption = @ServerName OR @ServerName = 'ALL')
						AND 
						ncp.ServerType LIKE '%FILESERVER%'
				  )
	BEGIN
		;THROW 50000, 'Specified @ServerName was not found.', 1;
	END

	--*************************************************************************************************
	-- declare table variable to hold server information
	--*************************************************************************************************

	DECLARE @ServerNames TABLE
	(
		ServerNamesID		INT IDENTITY(1,1),
		NodeID				INT,
		ServerName			VARCHAR(255)
	);

	--*************************************************************************************************
	--  Collect the ServerName values for the specified parameters
	--*************************************************************************************************
	INSERT INTO @ServerNames
		SELECT 
			n.NodeID,
			n.Caption
		FROM dbo.NodesData n
			INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
		WHERE ncp.ServerType LIKE '%FILESERVER%'
			AND (n.caption = @ServerName OR @ServerName = 'ALL')

	DECLARE @90DayTarget DATE = DATEADD(DAY, -90, GETDATE());
	DECLARE @60DayTarget DATE = DATEADD(DAY, -60, GETDATE());
	DECLARE @30DayTarget DATE = DATEADD(DAY, -30, GETDATE());
	DECLARE @10DayTarget DATE = DATEADD(DAY, -10, GETDATE());

	BEGIN TRY DROP TABLE #TempDiskStats END TRY BEGIN CATCH END CATCH;
	BEGIN TRY DROP TABLE #TempDiskStatsReportable END TRY BEGIN CATCH END CATCH;
	BEGIN TRY DROP TABLE #TempDiskProjection END TRY BEGIN CATCH END CATCH;
	BEGIN TRY DROP TABLE #TempVolumeUsage END TRY BEGIN CATCH END CATCH;
	BEGIN TRY DROP TABLE #TempFinalResults END TRY BEGIN CATCH END CATCH;

	CREATE TABLE #TempDiskStats
	(
		TempDiskStatsID			INT IDENTITY(1,1),
		NodeID					INT,
		NodeName				VARCHAR(256),
		VolumeID				INT,
		VolumeName				VARCHAR(256),
		VolumeSize				DECIMAL(18,3),
		VolumeUsed				DECIMAL(18,3),
		VolumeFreeSpace			DECIMAL(18,3),
		Day90DiskUsed			DECIMAL(18,3),
		Day90GrowthFactor		DECIMAL(18,12),
		Day90ProjectedUsage		DECIMAL(18,3),
		Day30DiskUsed			DECIMAL(18,3),
		Day30GrowthFactor		DECIMAL(18,12),
		Day30ProjectedUsage		DECIMAL(18,3),
		Day10DiskUsed			DECIMAL(18,3),
		Day10GrowthFactor		DECIMAL(18,12),
		Day10ProjectedUsage		DECIMAL(18,3),
		MaxProjectedUsage		DECIMAL(18,3),
		Reportable				BIT		DEFAULT 0,
		ReportableReason		VARCHAR(21) DEFAULT 'None'
	);

	CREATE TABLE #TempDiskStatsReportable
	(
		TempDiskStatsID			INT IDENTITY(1,1),
		NodeID					INT,
		NodeName				VARCHAR(256),
		VolumeID				INT,
		VolumeName				VARCHAR(256),
		VolumeSize				DECIMAL(18,3),
		VolumeUsed				DECIMAL(18,3),
		VolumeFreeSpace			DECIMAL(18,3),
		Day90DiskUsed			DECIMAL(18,3),
		Day90GrowthFactor		DECIMAL(18,12),
		Day90ProjectedUsage		DECIMAL(18,3),
		Day30DiskUsed			DECIMAL(18,3),
		Day30GrowthFactor		DECIMAL(18,12),
		Day30ProjectedUsage		DECIMAL(18,3),
		Day10DiskUsed			DECIMAL(18,3),
		Day10GrowthFactor		DECIMAL(18,12),
		Day10ProjectedUsage		DECIMAL(18,3),
		MaxProjectedUsage		DECIMAL(18,3),
		Reportable				BIT		DEFAULT 0,
		ReportableReason		VARCHAR(21) DEFAULT 'None'
	);

	CREATE TABLE #TempDiskProjection
	(
		NodeID				INT,
		VolumeID			INT,
		MaxProjectedUsage	DECIMAL(18,3)
	);

	CREATE TABLE #TempVolumeUsage
	(
		NodeID					INT,
		VolumeID				INT,
		[DateTime]				DATE,
		DiskSize				FLOAT,
		MaxDiskUsed				FLOAT,
		PercentDiskUsed			REAL
	);

	CREATE TABLE #TempFinalResults
	(
		NodeName				VARCHAR(256),
		VolumeName				VARCHAR(256),
		VolumeSize				DECIMAL(18,3),
		VolumeUsed				DECIMAL(18,3),
		VolumeFreeSpace			DECIMAL(18,3),
		VolumePercentFree		DECIMAL(5,2),
		ProjectedUsage			DECIMAL(18,3),
		Reportable				BIT,
		ReportableReason		VARCHAR(21),
		StatDate				DATE,
		DateVolumeSize			DECIMAL(18,3),
		DateVolumeUsed			DECIMAL(18,3),
		DateVolumeFreeSpace		DECIMAL(18,3),
		DateVolumePercentFree	DECIMAL(5,2)
	);

	--*************************************************************************************************
	-- Load volume list to temp table
	--*************************************************************************************************
	INSERT INTO #TempDiskStats (NodeID, NodeName, VolumeID, VolumeName, VolumeSize, VolumeUsed, VolumeFreeSpace)
		SELECT
			v.NodeID,
			sn.ServerName,
			v.VolumeID,
			v.Caption,
			v.VolumeSize / 1024/ 1024 / 1024,
			v.VolumeSpaceUsed / 1024 / 1024 / 1024,
			v.VolumeSpaceAvailable / 1024 / 1024 / 1024
		FROM dbo.Volumes v
			INNER JOIN @ServerNames sn ON sn.NodeID = v.NodeID
		WHERE v.VolumeTypeID = 4		-- fixed disk
		ORDER BY sn.ServerName, v.Caption;

	--*************************************************************************************************
	-- get 90-day data
	--*************************************************************************************************
	UPDATE #TempDiskStats SET
		Day90DiskUsed = vud.AvgDiskUsed / 1024 / 1024 / 1024
	FROM #TempDiskStats tds
		INNER JOIN dbo.VolumeUsage vud ON vud.NodeID = tds.NodeID AND vud.VolumeID = tds.VolumeID
	WHERE vud.[DateTime] = @90DayTarget;

	--*************************************************************************************************
	-- get 30-day data
	--*************************************************************************************************
	UPDATE #TempDiskStats SET
		Day30DiskUsed = vud.AvgDiskUsed / 1024 / 1024 / 1024
	FROM #TempDiskStats tds
		INNER JOIN dbo.VolumeUsage vud ON vud.NodeID = tds.NodeID AND vud.VolumeID = tds.VolumeID
	WHERE vud.[DateTime] = @30DayTarget;

	--*************************************************************************************************
	-- get 10-day data
	--*************************************************************************************************
	UPDATE #TempDiskStats SET
		Day10DiskUsed = vud.AvgDiskUsed / 1024 / 1024 / 1024
	FROM #TempDiskStats tds
		INNER JOIN dbo.VolumeUsage vud ON vud.NodeID = tds.NodeID AND vud.VolumeID = tds.VolumeID
	WHERE vud.[DateTime] = @10DayTarget;

	--*************************************************************************************************
	-- delete rows where we have no valid statistics
	--*************************************************************************************************
	DELETE FROM #TempDiskStats
	WHERE VolumeSize = 0 
		OR VolumeSize IS NULL
		OR Day90DiskUsed IS NULL
		OR Day30DiskUsed IS NULL
		OR Day10DiskUsed IS NULL

	--*************************************************************************************************
	-- delete rows where disk usage has not changed for 30 days
	--*************************************************************************************************
	DELETE FROM #TempDiskStats
	WHERE VolumeUsed = Day30DiskUsed

	--*************************************************************************************************
	-- calculate 90 day growth factor
	--*************************************************************************************************
	UPDATE #TempDiskStats SET
		Day90GrowthFactor = (VolumeUsed - Day90DiskUsed) / Day90DiskUsed / 90;

	--*************************************************************************************************
	-- calculate 30 day growth factor
	--*************************************************************************************************
	UPDATE #TempDiskStats SET
		Day30GrowthFactor = (VolumeUsed - Day30DiskUsed) / Day30DiskUsed / 30;

	-- calculate 10 day growth factor
	UPDATE #TempDiskStats SET
		Day10GrowthFactor = (VolumeUsed - Day10DiskUsed) / Day10DiskUsed / 10;

	--*************************************************************************************************
	-- calculate 90 day projected usage
	--*************************************************************************************************
	UPDATE #TempDiskStats SET
		Day90ProjectedUsage = VolumeUsed * POWER((1 + Day90GrowthFactor), 10),
		Day30ProjectedUsage = VolumeUsed * POWER((1 + Day30GrowthFactor), 10),
		Day10ProjectedUsage = VolumeUsed * POWER((1 + Day10GrowthFactor), 10);

	--*************************************************************************************************
	-- use CTE to get max projected usage
	--*************************************************************************************************
	;WITH cte (NodeID, VolumeID, ProjectedUsage)
	AS
	(
		SELECT
			NodeID,
			VolumeID,
			Day90ProjectedUsage
		FROM #TempDiskStats
		UNION
		SELECT
			NodeID,
			VolumeID,
			Day30ProjectedUsage
		FROM #TempDiskStats
		UNION
		SELECT
			NodeID,
			VolumeID,
			Day10ProjectedUsage
		FROM #TempDiskStats
	)
	INSERT INTO #TempDiskProjection
		SELECT
			NodeID,
			VolumeID,
			MAX(ProjectedUsage)
		FROM cte
		GROUP BY NodeID, VolumeID;

	--*************************************************************************************************
	-- Update temp table with max projected usage
	--*************************************************************************************************
	UPDATE #TempDiskStats SET
		MaxProjectedUsage = tdp.MaxProjectedUsage
	FROM #TempDiskStats tds
		INNER JOIN #TempDiskProjection tdp ON tdp.NodeID = tds.NodeID AND tdp.VolumeID = tds.VolumeID;

	--*************************************************************************************************
	-- set reportable flag
	--*************************************************************************************************
	UPDATE #TempDiskStats SET
		Reportable = 1,
		ReportableReason = 'Free Space Percentage'
	WHERE VolumeFreeSpace <= VolumeSize * .12;	-- less than 12% free space

	--*************************************************************************************************
	-- set reportable flag
	--*************************************************************************************************
	UPDATE #TempDiskStats SET
		Reportable = 1,
		ReportableReason = 'Projected Growth'
	WHERE (MaxProjectedUsage - VolumeUsed) > VolumeFreeSpace;	-- projected growth exceeds free space available

	--*************************************************************************************************
	-- copy reportable disks to new temp table
	-- We need to do it this way because, if we jsut delete the records from the #TempDiskStats table,
	--  it will mess up the identity column values when we loop through the table later.
	--*************************************************************************************************
	INSERT INTO #TempDiskStatsReportable
		SELECT
			NodeID,
			NodeName,
			VolumeID,
			VolumeName,
			VolumeSize,
			VolumeUsed,
			VolumeFreeSpace,
			Day90DiskUsed,
			Day90GrowthFactor,
			Day90ProjectedUsage,
			Day30DiskUsed,
			Day30GrowthFactor,
			Day30ProjectedUsage,
			Day10DiskUsed,
			Day10GrowthFactor,
			Day10ProjectedUsage,
			MaxProjectedUsage,
			Reportable,
			ReportableReason
		FROM #TempDiskStats
		WHERE Reportable = 1;

	IF (@DEBUG = 1)
	BEGIN
		SELECT * FROM #TempDiskStatsReportable;
	END;

	--*************************************************************************************************
	-- load temp table detail information on volumes for last 60 days
	--*************************************************************************************************
	INSERT INTO #TempVolumeUsage (NodeID, VolumeID, [DateTime], DiskSize, MaxDiskUsed, PercentDiskUsed)
		SELECT	
			vu.NodeID, 
			vu.VolumeID, 
			CAST([DateTime] AS DATE), 
			MAX(DiskSize) AS DiskSize, 
			MAX(MaxDiskUsed) AS MaxDiskUsed, 
			AVG(PercentDiskUsed) AS PercentDiskUsed 
		FROM dbo.VolumeUsage vu
			INNER JOIN #TempDiskStatsReportable tdsr ON tdsr.NodeID = vu.NodeID AND tdsr.VolumeID = vu.VolumeID
		WHERE [DateTime] >= @60DayTarget
		GROUP BY vu.NodeID, vu.VolumeID, CAST([DateTime] AS DATE)

	CREATE NONCLUSTERED INDEX IX_TempVolumeUsage ON #TempVolumeUsage (NodeID, VolumeID);

	IF (@DEBUG = 1)
	BEGIN
		SELECT * FROM #TempVolumeUsage;
	END;

	--*************************************************************************************************
	-- pull detail information on volumes for last 60 days
	--*************************************************************************************************
	PRINT 'Get Final Results...';
	DECLARE @X					INT = 1;
	DECLARE @RowCount			INT = (SELECT MAX(TempDiskStatsID) FROM #TempDiskStatsReportable);
	DECLARE @NodeID				INT = 0;
	DECLARE @NodeName			VARCHAR(256) = '';
	DECLARE @VolumeID			INT = 0;
	DECLARE @VolumeName			VARCHAR(256) = '';
	DECLARE @VolumeSize			DECIMAL(18,3) = 0.0;
	DECLARE @VolumeUsed			DECIMAL(18,3) = 0.0;
	DECLARE @VolumeFreeSpace	DECIMAL(18,3) = 0.0;
	DECLARE @VolumePercentFree	DECIMAL(5,2) = 0.0;
	DECLARE @MaxProjectedUsage	DECIMAL(18,3) = 0.0;
	DECLARE @Reportable			BIT = 0;
	DECLARE @ReportableReason	VARCHAR(21) = '';

	WHILE (@X <= @RowCount)
	BEGIN
		SELECT
			@NodeID = NodeID,
			@NodeName = NodeName,
			@VolumeID = VolumeID,
			@VolumeName = VolumeName,
			@VolumeSize = VolumeSize,
			@VolumeUsed = VolumeUsed,
			@VolumeFreeSpace = VolumeFreeSpace,
			@VolumePercentFree = VolumeFreeSpace / VolumeSize,
			@MaxProjectedUsage = MaxProjectedUsage,
			@Reportable = Reportable,
			@ReportableReason = ReportableReason
		FROM #TempDiskStatsReportable
		WHERE TempDiskStatsID = @X;

		IF (@DEBUG = 1)
		BEGIN
			PRINT '@NodeID = ' + CAST(@NodeID AS VARCHAR(30));
			PRINT '@NodeName = ' + @NodeName;
			PRINT '@VolumeID = ' + CAST(@VolumeID AS VARCHAR(30));
			PRINT '@VolumeName = ' + @VolumeName;
			PRINT '@VolumeSize = ' + CAST(@VolumeSize AS VARCHAR(30));
			PRINT '@VolumeUsed = ' + CAST(@VolumeUsed AS VARCHAR(30));
			PRINT '@VolumeFreeSpace = ' + CAST(@VolumeFreeSpace AS VARCHAR(30));
			PRINT '@VolumePercentFree = ' + CAST(@VolumePercentFree AS VARCHAR(30));
			PRINT '@MaxProjectedUsage = ' + CAST(@MaxProjectedUsage AS VARCHAR(30));
			PRINT '@Reportable = ' + CAST(@Reportable AS CHAR(1));
			PRINT '@ReportableReason = ' + @ReportableReason;
		END;

		INSERT INTO #TempFinalResults (NodeName, VolumeName, VolumeSize, VolumeUsed, VolumeFreeSpace, VolumePercentFree, ProjectedUsage, Reportable, ReportableReason, StatDate, DateVolumeSize, DateVolumeUsed, DateVolumeFreeSpace, DateVolumePercentFree)
			SELECT
				@NodeName,
				@VolumeName,
				@VolumeSize,
				@VolumeUsed,
				@VolumeFreeSpace,
				@VolumePercentFree,
				@MaxProjectedUsage,
				@Reportable,
				@ReportableReason,
				[DateTime],
				DiskSize / 1024 / 1024 / 1024,
				MaxDiskUsed / 1024 / 1024 / 1024,
				(DiskSize - MaxDiskUsed) / 1024 / 1024 / 1024,
				100 - PercentDiskUsed
			FROM #TempVolumeUsage tvu
			WHERE NodeID = @NodeID AND VolumeID = @VolumeID

		SET @X = @X + 1;
	END;

	--*************************************************************************************************
	-- select final results
	--*************************************************************************************************
	SELECT  
		NodeName,
		VolumeName,
		VolumeSize,
		VolumeUsed,
		VolumeFreeSpace,
		VolumePercentFree,
		ProjectedUsage,
		Reportable,
		ReportableReason,
		StatDate,
		DateVolumeSize,
		DateVolumeUsed,
		DateVolumeFreeSpace,
		DateVolumePercentFree
	FROM #TempFinalResults
	ORDER BY NodeName, VolumeName, StatDate;

	--*************************************************************************************************
	-- clean up temp tables
	--*************************************************************************************************
	BEGIN TRY DROP TABLE #TempDiskStats END TRY BEGIN CATCH END CATCH;
	BEGIN TRY DROP TABLE #TempDiskStatsReportable END TRY BEGIN CATCH END CATCH;
	BEGIN TRY DROP TABLE #TempDiskProjection END TRY BEGIN CATCH END CATCH;
	BEGIN TRY DROP TABLE #TempVolumeUsage END TRY BEGIN CATCH END CATCH;
	BEGIN TRY DROP TABLE #TempFinalResults END TRY BEGIN CATCH END CATCH;


END;
--SELECT * FROM #TempDiskStats

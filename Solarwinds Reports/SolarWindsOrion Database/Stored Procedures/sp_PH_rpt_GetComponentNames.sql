USE [SolarWindsOrion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[sp_PH_rpt_GetComponentNames]
	@ServerTier			VARCHAR(3) = 'ALL',
	@ServerName			VARCHAR(256) = 'ALL',
	@ApplicationName	VARCHAR(256) = NULL

/*=========================================================
NAME:			dbo.sp_PH_rpt_GetComponentNames
DESCRIPTION:	Retrieves the names of the components which
					have been defined for SQL-type servers
					and attached to at least one server.
					The list is restricted to a single
					application name if one is supplied,
INPUTS:			@ServerTier - the tier of the server(s) for which 
					to retrieve the component names.  A value of 
					'ALL' for this parameter retrieves component
					names for all server(s) which have an attahed
					component (restricted by the other parameters).
				@ServerName - name of the server for which
					the application names are to be retrieved.
					If the @ServerName parameter is 'ALL', then
					the list is not restricted to a specified
					server.
				@ApplicationName - name of the application
					for which component names are to be retrieved.
OUTPUTS:		None
RETURN:			None
RESULTS:		Resultset of the ApplicationNames
ERRORS:			50000 - Application name cannot be NULL
DEPENDENCIES:	dbo.APM_Application
				dbo.APM_Component
				dbo.NodesCustomProperties
				dbo.NodesData
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20171226	Initial Author
  rick.sellers@premisehealth.com	20180417	Changed to capture ALL database servers
									
=========================================================
--DEBUG--
DECLARE @ServerTier			VARCHAR(3) = 'ALL';
DECLARE @ServerName			VARCHAR(256) = 'ALL';
DECLARE @ApplicationName	VARCHAR(256) = '_DBA_Metrics_Collection - 1 Minute';

EXEC dbo.sp_PH_rpt_GetComponentNames @ServerTier, @ServerName, @ApplicationName;
    
=========================================================*/
AS
BEGIN
	IF (@ApplicationName IS NULL)
	BEGIN
		;THROW 50000, '@ApplicationName parameter cannot be NULL.', 1;
	END

	SELECT DISTINCT
		a.[Name] AS ApplicationName,
		c.[Name] AS ComponentName
	FROM dbo.APM_Component c 
		INNER JOIN dbo.APM_Application a ON a.ID = c.ApplicationID
		INNER JOIN dbo.NodesData n ON n.NodeID = a.NodeID
		INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
	WHERE ncp.ServerType LIKE '%SQL%'
		AND (ncp.ServerTier = @ServerTier OR @ServerTier = 'ALL')
		AND a.[Name] = @ApplicationName
		AND (n.Caption = @ServerName OR @ServerName = 'ALL')
	ORDER BY c.[Name];
END
USE [SolarWindsOrion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER  PROCEDURE [dbo].[sp_PH_rpt_GetJobFailures]
	@ServerTier				VARCHAR(3) = 'ALL',
	@ServerName				VARCHAR(256) = NULL,
	@StartDate				DATETIME = NULL,
	@StopDate				DATETIME = NULL

/*=========================================================
NAME:			dbo.sp_PH_rpt_GetJobFailures
DESCRIPTION:	Retrieves the triggered alerts for SQL Agent 
				Job failures from the SolarWindsOrion repository 
				database for the specified parameters.
INPUTS:			@ServerTier - the tier of the server(s) for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					server(s) to which this application is attached.
				@ServerName - the name of the server for which 
					to retrieve the data values.  A value of 'ALL'
					for this parameter retrieves metrics for all 
					server to which this application is attached.
				@StartDate - the starting date and time for the results
				@StopTime - the ending date and time for the results
OUTPUTS:		None
RETURN:			None
RESULTS:		Resultset of the triggered alerts for the specified parameters
ERRORS:			50000 - invalid server name
				50001 - @StartDate is not a valid date
				50002 - @StopDate is not a valid date
				50003 - invalid date range specified
DEPENDENCIES:	dbo.AlertHistory
				dbo.AlertObjects
				dbo.AlertConfigurations
				dbo.NodesCustomProperties
				dbo.NodesData
MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20180202	Initial Author
  rick.sellers@premisehealth.com	20180416	Condensed multiple alerts grouping by minute
  rick.sellers@premisehealth.com	20180417	Changed to capture ALL database servers
  rick.sellers@premisehealth.com	20180612	Suppress Long-Running jobs from report.
									
=========================================================
--DEBUG--

DECLARE @ServerTier				VARCHAR(3) = 'ALL';
DECLARE @ServerName				VARCHAR(256) = 'p-db-ohmuat-01';
DECLARE @StartDate				DATETIME = '2018-04-15 06:00';
DECLARE @StopDate				DATETIME = '2018-04-16 06:00';

EXEC dbo.sp_PH_rpt_GetJobFailures @ServerTier, @ServerName, @StartDate, @StopDate
    
=========================================================*/
AS
BEGIN
	--*************************************************************************************************
	--  Verify ServerName and that it is a SQL Server
	--*************************************************************************************************
	IF (@ServerName IS NULL) SET @ServerName = 'ALL';

	IF NOT EXISTS (SELECT 1 
					FROM dbo.NodesData n
						INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
					WHERE n.Caption = @ServerName OR @ServerName = 'ALL'
						AND ncp.ServerType LIKE '%SQL%'
				  )
	BEGIN
		;THROW 50000, 'Specified @ServerName was not found.', 1;
	END

	--*************************************************************************************************
	--  Verify date range
	--*************************************************************************************************
	IF (ISDATE(@StartDate)) <> 1
	BEGIN
		;THROW 50001, '@StartDate value is not a valid date.', 1;
	END

	IF (ISDATE(@StopDate)) <> 1
	BEGIN
		;THROW 50002, '@StopDate value is not a valid date.', 1;
	END

	IF (DATEDIFF(DAY, GETDATE(), @StopDate) > 1)
	BEGIN
		;THROW 50003, '@StopDate cannot be greater than today.', 1;
	END

	IF (@StopDate <= @StartDate)
	BEGIN
		;THROW 50004, '@StopDate must be >= @StartDate.', 1;
	END


	--*************************************************************************************************
	-- Declare a table variable of the appropriate servers based on the parameters
	--*************************************************************************************************
	DECLARE @ServerNames TABLE
	(
		ServerNamesID	INT IDENTITY(1,1),
		NodeID			INT,
		ServerName		VARCHAR(255)
	);

	INSERT INTO @ServerNames
		SELECT 
			n.NodeID,
			n.Caption
		FROM dbo.NodesData n
			INNER JOIN dbo.NodesCustomProperties ncp ON ncp.NodeID = n.NodeID
		WHERE ncp.ServerType LIKE '%SQL%'
			AND (ncp.ServerTier = @ServerTier OR @ServerTier = 'ALL')
			AND (n.caption = @ServerName OR @ServerName = 'ALL');

	--*************************************************************************************************
	--  Convert dates to DATETIMEOFFSET type for Central Time Zone to pull correct data
	--*************************************************************************************************
	DECLARE @StartDateUTC DATETIMEOFFSET = @StartDate AT TIME ZONE 'Central Standard Time';
	DECLARE @StopDateUTC DATETIMEOFFSET = @StopDate AT TIME ZONE 'Central Standard Time';

	--*************************************************************************************************
	--  Declare a temp table to hold the raw triggered alert records 
	--*************************************************************************************************
	IF EXISTS (SELECT 1 FROM tempdb.dbo.sysobjects WHERE xtype IN ('U') AND id = object_id(N'#AlertsRawData'))
	BEGIN
		DROP TABLE #AlertsRawData;
	END

	CREATE TABLE #AlertsRawData
	(
		NodeID				INT,
		ServerName			NVARCHAR(256),
		AlertMessage		NVARCHAR(MAX),
		EventTime			DATETIMEOFFSET(1),
		LocalTime			DATETIME,
		LocalTimeMinute		AS DATEADD(MINUTE, DATEDIFF(MINUTE, 0, LocalTime), 0), 
		AlertSeverityCode	INT,
		AlertSeverityDesc	VARCHAR(13),
		CountCritical		INT,
		CountSerious		INT,
		CountWarning		INT,
		CountNotice			INT,
		CountInformational	INT
	);


	--*************************************************************************************************
	--  Insert raw alert records into temp table 
	--*************************************************************************************************
	INSERT INTO #AlertsRawData (NodeID, ServerName, AlertMessage, EventTime, AlertSeverityCode, AlertSeverityDesc, CountCritical, CountSerious, CountWarning, CountNotice, CountInformational)
		SELECT
			sn.NodeID,
			sn.ServerName,
			ah.[Message] AS AlertMessage,
			ah.[TimeStamp] AT TIME ZONE 'Central Standard Time' AS EventTime,
			ac.Severity AS AlertSeverityCode,
			CASE ac.Severity
				WHEN 0 THEN 'Informational'
				WHEN 1 THEN 'Warning'
				WHEN 2 THEN 'Critical'
				WHEN 3 THEN 'Serious'
				WHEN 4 THEN 'Notice'
				ELSE 'Unknown'
			END AS AlertSeverityDesc,
			0,
			0,
			0,
			0,
			0
		FROM dbo.AlertHistory ah
			INNER JOIN dbo.AlertObjects ao ON ao.AlertObjectID = ah.AlertObjectID
			INNER JOIN dbo.AlertConfigurations ac ON ao.AlertID = ac.AlertID
			INNER JOIN @ServerNames sn ON sn.NodeID = ao.RelatedNodeId
		WHERE ah.EventType = 0 -- alert triggered 
			AND [TimeStamp] BETWEEN @StartDateUTC AND @StopDateUTC
			AND ah.[Message] LIKE 'SQL Agent Jobs:%'
			AND ah.[Message] NOT LIKE 'SQL Agent Jobs: Long-Running Jobs%';

	-- Calculate correct local time from UTC TimeStamp
	UPDATE #AlertsRawData SET LocalTime = DATEADD(MINUTE, DATEPART(tz,EventTime), EventTime);

	--*************************************************************************************************
	--  use CTEs to calculate counts and update temp table 
	--*************************************************************************************************
	;WITH Warnings (NodeID, WarningCount) 
	 AS
		(SELECT
			NodeID,
			COUNT(1) AS WarningCount
		FROM #AlertsRawData
		WHERE AlertSeverityCode = 1
		GROUP BY NodeID),
	 Criticals (NodeID, CriticalCount)
	 AS 
		(SELECT 
			NodeID,
			COUNT(1) AS CriticalCount
		FROM #AlertsRawData
		WHERE AlertSeverityCode = 2
		GROUP BY NodeID),
	 Informationals (NodeID, InformationalCount)
	 AS 
		(SELECT 
			NodeID,
			COUNT(1) AS InformationalCount
		FROM #AlertsRawData
		WHERE AlertSeverityCode = 0
		GROUP BY NodeID),
	 Notices (NodeID, NoticeCount)
	 AS 
		(SELECT 
			NodeID,
			COUNT(1) AS NoticeCount
		FROM #AlertsRawData
		WHERE AlertSeverityCode = 4
		GROUP BY NodeID),
	 Serious (NodeID, SeriousCount)
	 AS 
		(SELECT 
			NodeID,
			COUNT(1) AS SeriousCount
		FROM #AlertsRawData
		WHERE AlertSeverityCode = 3
		GROUP BY NodeID)
	UPDATE #AlertsRawData SET
		CountWarning = ISNULL(w.WarningCount,0),
		CountCritical = ISNULL(c.CriticalCount,0),	
		CountInformational = ISNULL(i.InformationalCount,0),	
		CountNotice = ISNULL(n.NoticeCount,0),	
		CountSerious = ISNULL(s.SeriousCount,0)	
	FROM #AlertsRawData ard
		LEFT OUTER JOIN Warnings w ON w.NodeID = ard.NodeID
		LEFT OUTER JOIN Criticals c ON c.NodeID = ard.NodeID
		LEFT OUTER JOIN Informationals i ON i.NodeID = ard.NodeID
		LEFT OUTER JOIN Notices n ON n.NodeID = ard.NodeID
		LEFT OUTER JOIN Serious s ON s.NodeID = ard.NodeID;

	--*************************************************************************************************
	--  Select final results from temp table 
	--*************************************************************************************************
	SELECT DISTINCT
		NodeID, 
		ServerName,
		AlertMessage,
		LocalTimeMinute,
		AlertSeverityCode,
		AlertSeverityDesc,
		CountCritical,
		CountSerious,
		CountWarning,
		CountNotice,
		CountInformational
	FROM #AlertsRawData
	ORDER BY ServerName ASC, LocalTimeMinute DESC;
END;
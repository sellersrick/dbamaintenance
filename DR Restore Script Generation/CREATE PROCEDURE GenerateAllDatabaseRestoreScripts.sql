

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'GenerateAllDatabaseRestoreScripts' AND [type] = 'P')
BEGIN
	DROP PROCEDURE GenerateAllDatabaseRestoreScripts
END
GO

CREATE PROCEDURE dbo.GenerateAllDatabaseRestoreScripts
	@RestoreDataFolder		VARCHAR(2000),
	@RestoreLogFolder		VARCHAR(2000),
	@StopAt					DATETIME,
	@IsReplace				BIT = 0,
	@RunCheckDB				BIT = 0
/*=========================================================
NAME:			dbo.GenerateAllDatabaseRestoreScripts
DESCRIPTION:	Loops through all user databases and calls prcedure
				dbo.GenerateDatabaseRestoreScripts for each one 
				using the parameters provided.
INPUTS:			None
OUTPUTS:		None
RETURN:			None
RESULTS:		None
ERRORS:			No Custom Errors
DEPENDENCIES:	User databases
				dbo.DatabaseRestoreCommands

MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20200324	Initial Author
									
*************************************************************************
-- DEBUG
DECLARE @RestoreDataFolder		VARCHAR(2000);
DECLARE @RestoreLogFolder		VARCHAR(2000);
DECLARE @StopAt					DATETIME;
DECLARE @IsReplace				BIT = 1;
DECLARE @RunCheckDB				BIT = 0;

SET @RestoreDataFolder = NULL --'C:\TempData';
SET @RestoreLogFolder = NULL --'C:\TempLog';
SET @StopAt = NULL --'03/17/2020 11:35:00';
SET @IsReplace = 1;
SET @RunCheckDB = 0;

EXEC dbo.GenerateAllDatabaseRestoreScripts @RestoreDataFolder, @RestoreLogFolder, @StopAt, @IsReplace, @RunCheckDB

*************************************************************************/
AS
SET NOCOUNT ON;

-- Empty the current table
TRUNCATE TABLE dbo.DatabaseRestoreCommands;

DECLARE @Databases TABLE
(
	DatabasesID		INT IDENTITY(1,1),
	DatabaseName	VARCHAR(128)
)

INSERT INTO @Databases
	SELECT [name]
	FROM sys.databases
	WHERE [database_id] BETWEEN 5 AND 32766
		AND state_desc <> 'OFFLINE'

DECLARE @X	INT = 1
DECLARE @RowCount INT = (SELECT MAX(DatabasesID) FROM @Databases)
DECLARE @DatabaseName	VARCHAR(128)

WHILE (@X <= @RowCount)
BEGIN
	SELECT @DatabaseName = DatabaseName FROM @Databases WHERE DatabasesID = @X
	PRINT 'Processing database ' + @DatabaseName

	EXEC dbo.GenerateDatabaseRestoreScript @DatabaseName, NULL, @RestoreDataFolder, @RestoreLogFolder, @StopAt, @IsReplace, @RunCheckDB

	SET @X += 1
END


BEGIN TRY DROP TABLE DatabaseRestoreCommands END TRY BEGIN CATCH END CATCH
GO

CREATE TABLE dbo.DatabaseRestoreCommands
(
	DatabaseRestoreCommandsID	INT	IDENTITY(1,1),
	DatabaseName				VARCHAR(128) NOT NULL,
	RestoreCommandID			INT NOT NULL,
	Command						VARCHAR(4000)
)
GO

ALTER TABLE dbo.DatabaseRestoreCommands ADD CONSTRAINT PK_DatabaseRestoreCommands PRIMARY KEY CLUSTERED (DatabaseRestoreCommandsID)
GO

CREATE NONCLUSTERED INDEX IX_DatabaseRestoreCommands_DatabaseName_RestoreCommandID ON dbo.DatabaseRestoreCommands (DatabaseName, RestoreCommandID)
GO


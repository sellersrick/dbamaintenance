

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'GenerateDatabaseRestoreScript' AND [type] = 'P')
BEGIN
	DROP PROCEDURE GenerateDatabaseRestoreScript
END
GO

CREATE PROCEDURE [dbo].[GenerateDatabaseRestoreScript]
	@DatabaseName			NVARCHAR(128),
	@NewDatabaseName		NVARCHAR(128) = NULL,
	@RestoreDataFolder		VARCHAR(2000) = NULL,
	@RestoreLogFolder		VARCHAR(2000) = NULL,
	@StopAt					DATETIME = NULL,
	@IsReplace				BIT = 0,
	@RunCheckDB				BIT = 0
/*=========================================================
NAME:			dbo.GenerateDatabaseRestoreScript
DESCRIPTION:	Generates the commands to restore a database from
					the available backups.  Provides the ability to 
					restore the database under a different name, 
					move the files to a different folder, replace the 
					current database, restore to a specific date and
					time, and run a DBCC CHECKDB after the restore has 
					finished.
INPUTS:			None
OUTPUTS:		None
RETURN:			None
RESULTS:		None
ERRORS:			No Custom Errors
DEPENDENCIES:	master.sys.databases
				master.sys.master_files
				msdb.dbo.backupmediafamily
				msdb.dbo.backupset

MODIFICATIONS:
  EMAIL								DATE		DESC
  rick.sellers@premisehealth.com	20170424	Initial Author
  rick.sellers@premisehealth.com	20180522	Fixed StopAt functionality
  rick.sellers@premisehealth.com	20200320	Miscellaneous tweaks
  rick.sellers@premisehealth.com	20200324	Write output to permanent table
									
*************************************************************************
-- DEBUG
DECLARE @DatabaseName			NVARCHAR(128);
DECLARE @NewDatabaseName		NVARCHAR(128);
DECLARE @RestoreDataFolder		VARCHAR(2000);
DECLARE @RestoreLogFolder		VARCHAR(2000);
DECLARE @StopAt					DATETIME;
DECLARE @IsReplace				BIT = 1;
DECLARE @RunCheckDB				BIT = 1;

SET @DatabaseName = N'ReportServer';
SET @NewDatabaseName = N'RickWork'
SET @RestoreDataFolder = 'C:\TempData';
SET @RestoreLogFolder = 'C:\TempLog';
SET @StopAt = '03/18/2020 01:35:00';
SET @IsReplace = 0;
SET @RunCheckDB = 1

EXEC dbo.GenerateDatabaseRestoreScript @DatabaseName, @NewDatabaseName, @RestoreDataFolder, @RestoreLogFolder, @StopAt, @IsReplace, @RunCheckDB

*************************************************************************/
AS
SET NOCOUNT ON;

BEGIN TRY
	DECLARE @Commands				NVARCHAR(MAX) = N'';
	DECLARE @X						INT = 1;
	DECLARE @RowCount				INT = 0;
	DECLARE @Y						INT = 1;
	DECLARE @RowCount2				INT = 0;

	DECLARE @LogicalName			NVARCHAR(128) = N'';
	DECLARE @PhysicalName			NVARCHAR(260) = N'';
	DECLARE @PhysicalPath			NVARCHAR(2000) = N'';
	DECLARE @BackupType				CHAR(1) = N'';
	DECLARE @PhysicalDeviceName		NVARCHAR(2000) = N'';
	DECLARE @FileType				NVARCHAR(60) = N'';
	DECLARE @Position				INT = 0;
	DECLARE @BackupChecksums		BIT = 0;


	DECLARE @BackupFiles TABLE
	(
		BackupFilesID			INT IDENTITY(1,1),
		DatabaseName			NVARCHAR(128) NOT NULL,
		CompatibilityLevel		TINYINT NOT NULL,
		Last_LSN				NUMERIC(25,0) NOT NULL,
		IsReadOnly				BIT NOT NULL,
		StateDesc				NVARCHAR(60) NOT NULL,
		RecoveryModelDesc		NVARCHAR(60) NOT NULL,
		BackupChecksums			BIT NOT NULL,
		BackupSize				NUMERIC(20,0) NOT NULL,
		BackupType				CHAR(1) NOT NULL,
		MediaSetID				INT NOT NULL,
		FamilySequenceNbr		TINYINT NOT NULL,
		BackupFinishDate		DATETIME NOT NULL,
		PhysicalDeviceName		NVARCHAR(2000) NOT NULL,
		Position				INT NOT NULL
	);

	DECLARE @LogicalFiles TABLE
	(
		LogicalFilesID		INT IDENTITY(1,1),
		LogicalName			NVARCHAR(128) NOT NULL,
		PhysicalName		NVARCHAR(256) NOT NULL,
		PhysicalPath		NVARCHAR(2000) NOT NULL,
		FileType			NVARCHAR(60) NOT NULL
	);

	DECLARE @CommandsTable TABLE
	(
		CommandID			INT IDENTITY(1,1),
		Command				NVARCHAR(MAX) NOT NULL
	);

	-- get out if database name was not specified
	IF (@DatabaseName IS NULL)
	BEGIN
		RAISERROR ('@DatabaseName parameter cannot be NULL', 16, 1);
	END;

	-- give @NewDatabaseName the same vaue as @DatabaseName if it is NULL
	IF (@NewDatabaseName IS NULL)
	BEGIN
		SET @NewDatabaseName = @DatabaseName
	END

	-- verify that @RestoreDataFolder and @RestoreLogFolder are both NULL or both have values
	IF (@RestoreDataFolder IS NULL AND @RestoreLogFolder IS NOT NULL) 
	BEGIN
		RAISERROR ('@RestoreLogFolder cannot be specified when @RestoreDataFolder is NULL', 16, 1);
	END
	IF (@RestoreLogFolder IS NULL AND @RestoreDataFolder IS NOT NULL) 
	BEGIN
		RAISERROR ('@RestoreDataFolder cannot be specified when @RestoreLogFolder is NULL', 16, 1);
	END

	-- Set StopAt time to current time if not specified
	IF (@StopAt IS NULL) SET @StopAt = GETDATE();

	-- get last FULL backup file
	INSERT INTO @BackupFiles
		SELECT TOP (1)
			bs.[database_name],
			d.[compatibility_level],
			bs.last_lsn,
			d.is_read_only,
			d.state_desc,
			d.recovery_model_desc,
			bs.has_backup_checksums,
			bs.backup_size,
			'D',	-- type
			bs.media_set_id,
			bmf.family_sequence_number,
			bs.backup_finish_date,
			bmf.physical_device_name,
			bs.position
		FROM msdb.dbo.backupset bs
			INNER JOIN master.sys.databases d 
				ON d.[name] = bs.[database_name]
			INNER JOIN master.sys.master_files mf
				ON d.database_id = mf.database_id
					AND mf.[type_desc] = 'ROWS'
			INNER JOIN msdb.dbo.backupmediafamily bmf
				ON bs.media_set_id = bmf.media_set_id
		WHERE bs.[type] = 'D'
			AND bs.[database_name] = @DatabaseName
			AND bs.backup_finish_date <= @StopAt
			AND bs.is_copy_only = 0
			AND bmf.device_type = 2 -- disk backup
		ORDER BY bs.backup_finish_date DESC;

	-- get last DIFF backup file
	INSERT INTO @BackupFiles
		SELECT TOP (1)
			bs.[database_name],
			d.[compatibility_level],
			bs.last_lsn,
			d.is_read_only,
			d.state_desc,
			d.recovery_model_desc,
			bs.has_backup_checksums,
			bs.backup_size,
			'I',	-- type
			bs.media_set_id,
			bmf.family_sequence_number,
			bs.backup_finish_date,
			bmf.physical_device_name,
			bs.position
		FROM msdb.dbo.backupset bs
			INNER JOIN master.sys.databases d 
				ON d.[name] = bs.[database_name]
			INNER JOIN master.sys.master_files mf
				ON d.database_id = mf.database_id
					AND mf.[type_desc] = 'ROWS'
			INNER JOIN msdb.dbo.backupmediafamily bmf
				ON bs.media_set_id = bmf.media_set_id
		WHERE bs.[type] = 'I'
			AND bs.[database_name] = @DatabaseName
			AND bs.backup_finish_date <= @StopAt
			AND bs.is_copy_only = 0
			AND bs.backup_finish_date >= (SELECT MAX(BackupFinishDate) FROM @BackupFiles)
			AND bmf.device_type = 2 -- disk backup
		ORDER BY bs.backup_finish_date DESC;

	-- get all log backups since FULL/Diff backup finished and before @StopAt time
	INSERT INTO @BackupFiles
		SELECT
			bs.[database_name],
			d.[compatibility_level],
			bs.last_lsn,
			d.is_read_only,
			d.state_desc,
			d.recovery_model_desc,
			bs.has_backup_checksums,
			bs.backup_size,
			'L',	-- type
			bs.media_set_id,
			bmf.family_sequence_number,
			bs.backup_finish_date,
			bmf.physical_device_name,
			bs.position
		FROM msdb.dbo.backupset bs
			INNER JOIN master.sys.databases d 
				ON d.[name] = bs.[database_name]
			INNER JOIN master.sys.master_files mf
				ON d.database_id = mf.database_id
					AND mf.[type_desc] = 'LOG'
			INNER JOIN msdb.dbo.backupmediafamily bmf
			  ON bs.media_set_id = bmf.media_set_id
				AND bmf.family_sequence_number BETWEEN bs.first_family_number AND bs.last_family_number
		WHERE bs.[type] = 'L'
			AND bs.[database_name] = @DatabaseName
			AND bs.backup_finish_date <= @StopAt
			AND bs.is_copy_only = 0
			AND bs.backup_finish_date >= (SELECT MAX(BackupFinishDate) FROM @BackupFiles)
			AND bmf.device_type = 2 -- disk backup
		ORDER BY bs.backup_finish_date ASC;

	-- get last log backup to cover @StopAt time
	INSERT INTO @BackupFiles
		SELECT TOP (1)
			bs.[database_name],
			d.[compatibility_level],
			bs.last_lsn,
			d.is_read_only,
			d.state_desc,
			d.recovery_model_desc,
			bs.has_backup_checksums,
			bs.backup_size,
			'L',	-- type
			bs.media_set_id,
			bmf.family_sequence_number,
			bs.backup_finish_date,
			bmf.physical_device_name,
			bs.position
		FROM msdb.dbo.backupset bs
			INNER JOIN master.sys.databases d 
				ON d.[name] = bs.[database_name]
			INNER JOIN master.sys.master_files mf
				ON d.database_id = mf.database_id
					AND mf.[type_desc] = 'LOG'
			INNER JOIN msdb.dbo.backupmediafamily bmf
			  ON bs.media_set_id = bmf.media_set_id
				AND bmf.family_sequence_number BETWEEN bs.first_family_number AND bs.last_family_number
		WHERE bs.[type] = 'L'
			AND bs.[database_name] = @DatabaseName
			AND bs.backup_finish_date >= @StopAt
			AND bs.is_copy_only = 0
			AND bs.backup_finish_date >= (SELECT MAX(BackupFinishDate) FROM @BackupFiles)
			AND bmf.device_type = 2 -- disk backup
		ORDER BY bs.backup_finish_date ASC;

	--SELECT * FROM @BackupFiles;

	-- load logical files table
	INSERT INTO @LogicalFiles
		SELECT 
			mf.[name],
			SUBSTRING(mf.physical_name,LEN(mf.physical_name) - CHARINDEX('\',REVERSE(mf.physical_name),1) + 2,CHARINDEX('\',REVERSE(mf.physical_name),1) + 1),
			mf.physical_name,
			mf.[type_desc] 
		FROM master.sys.databases d
			INNER JOIN master.sys.master_files mf ON d.database_id = mf.database_id
		WHERE d.[name] = @DatabaseName
			AND mf.[type_desc] IN ('ROWS','LOG')

	--SELECT * FROM @LogicalFiles;

	SET @RowCount = (SELECT MAX(BackupFilesID) FROM @BackupFiles);
	SET @RowCount2 = (SELECT MAX(LogicalFilesID) FROM @LogicalFiles);

	IF (@RowCount > 0)
	BEGIN
		WHILE (@X <= @RowCount)
		BEGIN
			SELECT
				@BackupChecksums = BackupChecksums,
				@Position = Position,
				@BackupType	= BackupType,
				@PhysicalDeviceName	= PhysicalDeviceName
			FROM @BackupFiles
			WHERE BackupFilesID = @X;

			IF (@BackupType = 'D')	-- FULL Backups
			BEGIN
				SET @Commands = 
				N'RESTORE DATABASE [' + @NewDatabaseName + '] FROM DISK=''' + @PhysicalDeviceName + ''' WITH ' + CHAR(10);

				SET @Y = 1;
				WHILE (@Y <= @RowCount2)
				BEGIN
					SELECT
						@LogicalName = LogicalName,
						@PhysicalName = PhysicalName,
						@PhysicalPath = PhysicalPath,
						@FileType = FileType
					FROM @LogicalFiles
					WHERE LogicalFilesID = @Y;

					-- override physical file name if database name is being changed
					IF (@DatabaseName <> @NewDatabaseName)
					BEGIN
						IF(@FileType = 'Rows')
						BEGIN
							SET @PhysicalName = @NewDatabaseName + '.mdf'
						END

						IF(@FileType = 'LOG')
						BEGIN
							SET @PhysicalName = @NewDatabaseName + '_log.ldf'
						END
						SET @PhysicalPath = SUBSTRING(@PhysicalPath, 1, LEN(@PhysicalPath) - CHARINDEX('\', REVERSE(@PhysicalPath), 1))
					END

					IF (@RestoreDataFolder IS NULL)
					BEGIN
						SET @Commands = @Commands + N'MOVE ''' + @LogicalName + ''' TO ''' + @PhysicalPath + ''', ' + CHAR(10);
					END
					ELSE
					BEGIN
						IF (@FileType = 'ROWS')
						BEGIN
							SET @Commands = @Commands + N'MOVE ''' + @LogicalName + ''' TO ''' + @RestoreDataFolder + N'\' + @PhysicalName + ''', ' + CHAR(10);
						END
						ELSE IF (@FileType = 'LOG')
						BEGIN
							SET @Commands = @Commands + N'MOVE ''' + @LogicalName + ''' TO ''' + @RestoreLogFolder + N'\' + @PhysicalName + ''', ' + CHAR(10);
						END;
					END
					SET @Y = @Y + 1;
				END

				SET @Commands = @Commands + N'NORECOVERY, FILE = ' + CAST(@Position AS VARCHAR(5)) + ', ' +
					CASE @BackupChecksums WHEN 1 THEN 'CHECKSUM, ' ELSE '' END + N'STATS=1' + CASE @IsReplace WHEN 1 THEN ', REPLACE' ELSE '' END + CHAR(10);
				INSERT INTO @CommandsTable VALUES(@Commands);
			END
			ELSE IF (@BackupType = 'I')	-- Differential Backups
			BEGIN
				SET @Commands = 
					N'RESTORE DATABASE [' + @NewDatabaseName + '] FROM DISK=''' + @PhysicalDeviceName + ''' WITH NORECOVERY, FILE = ' + CAST(@Position AS VARCHAR(5)) + ', STATS=1' +
					CASE @BackupChecksums WHEN 1 THEN ', CHECKSUM' ELSE '' END + CHAR(10);
				INSERT INTO @CommandsTable VALUES(@Commands);
			END
			ELSE IF (@BackupType = 'L')	-- Log Backups
			BEGIN
				SET @Commands = 
					N'RESTORE LOG [' + @NewDatabaseName + '] FROM DISK=''' + @PhysicalDeviceName + ''' WITH NORECOVERY, FILE = ' + CAST(@Position AS VARCHAR(5)) + ', STATS=1' +
					CASE @BackupChecksums WHEN 1 THEN ', CHECKSUM' ELSE '' END;
				INSERT INTO @CommandsTable VALUES(@Commands);
			END
			ELSE
			BEGIN
				DECLARE @ErrorMsg NVARCHAR(255);
				SET @ErrorMsg = N'Invalid @BackupType value encountered (' + @BackupType + ')';
				RAISERROR (@ErrorMsg, 16, 1);
			END

			SET @X = @X + 1;
		END

		-- Append StopAt datetime value to last RESTORE LOG command
		DECLARE @LastLogBackupID INT = (SELECT MAX(CommandID) FROM @CommandsTable WHERE Command LIKE 'RESTORE LOG%')
		IF (@LastLogBackupID IS NOT NULL)
		BEGIN
			UPDATE @CommandsTable SET Command = Command + ', STOPAT = ''' + CAST(@StopAt AS VARCHAR(30)) + '''' WHERE CommandID = @LastLogBackupID;
		END


		SET @Commands = N'RESTORE DATABASE [' + @NewDatabaseName + '] WITH RECOVERY';
		INSERT INTO @CommandsTable VALUES(@Commands);
		INSERT INTO @CommandsTable VALUES('GO' + CHAR(10))
		IF (@RunCheckDB = 1)
		BEGIN
			SET @Commands = N'PRINT ''Running CHECKDB'';';
			INSERT INTO @CommandsTable VALUES(@Commands);
			SET @Commands = N'USE [' + @NewDatabaseName + ']; DBCC CHECKDB(0) WITH NO_INFOMSGS, EXTENDED_LOGICAL_CHECKS;' + CHAR(10);
			INSERT INTO @CommandsTable VALUES(@Commands);
		END

		-- ensure that we do not have any existing records for this database
		DELETE FROM dbo.DatabaseRestoreCommands WHERE DatabaseName = @NewDatabaseName

		-- insert commands into permanent table
		INSERT INTO dbo.DatabaseRestoreCommands
			SELECT 
				@NewDatabaseName,
				CommandID,
				Command 
			FROM @CommandsTable 
			ORDER BY CommandID;
	END
	ELSE
	BEGIN
		PRINT '   No backup files are available for database ' + @DatabaseName
	END
END TRY 
BEGIN CATCH
	PRINT 'Error encountered.  Error Number = ' + CAST(ERROR_NUMBER() AS VARCHAR(10)) + CHAR(10)
	PRINT 'Error Message: ' + ISNULL(ERROR_MESSAGE(), 'No Error Message Available.')
END CATCH

